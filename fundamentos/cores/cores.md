# Cores

A aplicação correta da paleta de cores visa trazer consistência ao *Design System* e contribuir para garantir uma interface unificada e facilmente reconhecível pelos seus usuários. É fundamental que a paleta esteja alinhada aos Princípios do *Design System* e seja aplicada corretamente na biblioteca de componentes contribuindo para o equilíbrio entre os diversos produtos e sistemas do governo.

---

## Princípios

### Experiência Única

A paleta do Design System é constituída e organizada por cores e famílias de cores, abrangendo dimensões de matizes, brilhos e saturação. Além disso, são estabelecidas finalidades específicas para algumas dessas famílias: *Cores de Superfície*, *Cores de Leitura*, *Cores Interativas* e *Cores de Feedback*. Elas tem o objetivo de preservar a identidade visual e manter a mesma experiência em todos os produtos e sistemas do Governo Federal.

> *Exemplo:* Nesse sistema, a família *Blue Warm Vivid*, que contém a cor *Blue Warm Vivid 70*, foram as cores selecionadas para representar a identidade do Governo Federal.

### Eficiência e Clareza

A cor deve indicar o estado e hierarquia dos elementos dispostos em uma interface, facilitando o *feedback* e comunicação entre a interface e o usuário, enquanto se mantêm consistentes ao longo de toda a navegação.

> *Exemplo:* A cor ajuda a indicar quais elementos são interativos, como eles se relacionam com outros elementos.

### Acessibilidade

Um sistema de serviço deve cumprir as diferentes necessidades de acessibilidade. Para tanto, as cores existentes na paleta foram selecionadas para se obter um mínimo de contraste e garantir boa legibilidade nas interfaces digitais.

A [WCAG 2.1](https://www.w3.org/TR/WCAG20/) (Diretrizes de Acessibilidade para o Conteúdo da Web) descreve que existem vários níveis de conformidade (A, AA ou AAA), de forma que, quanto mais itens são atendidos, maior será o nível ou suporte de acessibilidade na interface.

O contraste varia entre 1 a 21 (geralmente descritos 1:1 a 21:1).

O *nível AA* indica que é necessária uma taxa de contraste de pelo menos *4,5:1* para que o texto seja considerado normal e tenha boa legibilidade. Para texto grande a taxa de contraste mínimo deve ser de pelo menos *3:1*.

O *nível AAA* requer uma taxa de contraste de pelo menos *7:1* para texto normal e *4,5:1* para texto grande.

É considerado *texto grande* aquele em que a fonte possui pelo menos 18 pontos (24px, 1,71em, 171% do tamanho padrão da Fonte Base, veja [Tipografia](https://www.gov.br/fundamentos-visuais/tipografia) para maiores detalhes) ou 14 pontos em negrito (19px, 1,325em, 132,5%), um texto com tamanho abaixo desses valores (tal como 14pt regular) é considerado *texto normal*.

Elementos gráficos e componentes da interface (como ícones e bordas) seguem as mesmas regras de texto grande.  

O *Design System* do governo trabalha com nível de conformidade *mínima de AA* (na taxa de contraste *4,5:1*), pois permite uma flexibilidade maior de variações de cores (em relação à conformidade AAA) e atende aos critérios mínimos de contraste proposto pela WCAG 2.1.

*Observação*: algumas cores foram selecionadas para um uso bastante específico, deste modo, elas funcionam no contraste mínimo de 3:1, ou seja, apenas para uso em elementos gráficos (ou texto grande).

> Para testar a acessibilidade das cores foram utilizadas as seguintes ferramentas online [Contrast Checker](https://contrastchecker.com) e [Web AIM Contrast Checker](https://webaim.org/resources/contrastchecker)

Para o *Design System* foi analisado a relação de contraste entre *a cor aplicada aos textos (2 variações que dependendo do brilho do plano de fundo da interface: claro ou escuro) e as cores que compõem a paleta do DS*.

> *Exemplo:* Para o texto em *fundo claro* optou-se pelo uso do cinza *#333333* que possui contraste inferior ao preto, mas suficiente para uma boa leitura na tela. O tom neutro, deixa a composição mais leve, tornando a leitura mais agradável. Para texto em *fundo escuro* optou-se pela cor branca *#FFFFF*.

### Reutilização e Colaboração

Interfaces digitais do governo devem utilizar sempre as cores pré-definidas. Havendo a necessidade de acrescentar uma nova cor à paleta do DS, faz-se necessário que a cor seja testada do ponto de vista de acessibilidade e passe por uma prévia aprovação da equipe de design do DS.

*OBS:* Consulte a acessibilidade das cores usadas no Design System na [Tabela de Contraste](/fundamentos-visuais/cores#contrast-table).

---

## Sistema de Cores

O sistema de cores utilizado no Design System, assim como o modelo de gradação em luminância e *Famílias de Cores*, foram adaptados do modelo de cores do [U.S. Web Design System (USWDS)](https://designsystem.digital.gov/).

### Cor

É qualquer amostra específica em nossas famílias de cores. Elas são representadas por uma nomenclatura construída com base na família e luminância. Por exemplo:  `Red 50`, `Blue Warm Vivid 70`, `Indigo Cool Vivid 5`, etc.

### Família de Cores

É um grupo de cores que têm o mesmo matiz em um círculo cromático. Eles contêm várias cores individuais, diferenciadas pelo brilho ou saturação de cada uma delas. Uma família de cores é normalmente representada por um nome que faz referência a sua matiz, como por exemplo: `Red`, `Blue Warm`, `Green Cool`, etc.

As famílias de cores do Design System são escolhidas a partir do modelo *HSL* *(Hue, Saturation, Lightness)*, sistema de cores que utiliza os parâmetros de matiz, saturação e brilho, sendo dispostas em 360º em um círculo cromático. Desta forma, é possível rotular cada família através de sua posição em graus, neste modelo. As cores são selecionadas seguindo a mesma lógica de gradação de luminância em todas as famílias.

### Família de Cores Alternativas

Refere-se ao mesmo conceito de *Família de Cores*. Fazem parte de um grupo de cores de um mesmo matiz mas pertencentes a um outro círculo cromático (com característica diferente da família "padrão"). Sua nomenclatura tem a mesma base da *Família de Cores* adicionando um sufixo.

Atualmente temos apenas um círculo cromático alternativo chamado `Vivid`, criando famílias como `Red Vivid`, `Blue Warm Vivid`, `Green Cool Vivid`, etc.

### Círculo Cromático

Os círculos cromáticos são uma maneira de exibir as famílias do sistema de cores do Design System, dispostas em 360° no modelo de cores HSL. Cada família de cores está localizada com sua posição (em graus) neste modelo.

Abaixo é possível observar o padrão das famílias de cores do Design System dentro de cada círculo cromático.

![Sistema HSL - Famílias de Cores](imagens/circle-01.png)
*Círculo cromático (padrão)*

![Sistema HSL - Famílias de Cores Alternativas](imagens/circle-02.png)
*Círculo cromático vivid (alternativo) -  cores mais saturadas.*

Existem algumas famílias que não estão visíveis dentro dos círculos cromáticos acima mas que podem ser usados como cores neutras:

- Famílias de Cinza: `Gray`, `Gray-Cool` e `Gray-Warm`.
- Família Pura: `Pure`

A lista de todas as cores disponíveis pode ser encontradas na [Paleta do Design System](/fundamentos-visuais/cores#paleta-do-govbr-ds).

---

## Função das Cores

O Design System estabelece finalidades específicas utilizando as famílias de cores, com o intuito de facilitar a lógica para criação de temas. Em cada função estabelecida, qualquer cor da família especificada poderá ser usada, porém é recomendado utilizar a cor principal ou as alternativas indicadas nas escalas de cores.

**OBS:** Em algumas situações, poderá ser observado o uso 2 de famílias para um determinada função e aplicação de fundo. Isso acontece por causa do uso da *Família Pure*, ela funciona como uma "família de apoio" para as demais. Maiores informações veja Paleta do Design System.

### Cor Principal

É a recomendação de uso principal que poderá ser usada para as finalidades estabelecidas. São representadas pela letra *P* e indicadas na *Família de Cores* da seguinte forma:

![Exemplo de Indicação - Cor Principal](imagens/sample-default.png)
*Exemplo de Indicação - Cor Principal*

### Cor Alternativa

São alternativas de cores recomendadas que podem ser usadas quando não for possível utilizar a recomendação principal, ou trabalhar com ênfases. São representadas pela letra *A* e indicadas na *Família de Cores* da seguinte forma:

![Exemplo de Indicação - Cor Alternativa](imagens/sample-alternative.png)
*Exemplo de Indicação - Cor Alternativa*

### Fundo Claro Vs Fundo Escuro

Quando se escolhe uma cor para um determinado elemento ou função, geralmente existe uma superfície comum da qual será aplicado aquele elemento. Porém, em algumas situações, a luminância desse fundo pode ser diferente do habitual, gerando contraste insuficiente (entre o fundo e o elemento) para se perceber o elemento.

Sempre que escolher uma cor (seja a principal ou as alternativas), determine o uso dela em diferentes luminâncias. Para facilitar, podemos pensar no uso desses elementos em diferentes superfícies: claro e escuro.

Abaixo estão listadas as principais funções das cores, as recomendações de qual família de cor utilizar, assim como a recomendação da cor principal e das alternativas dentro do tema base.

**OBS:** Cada função está ligada a um ou mais fundamentos. Procure as documentações relacionadas para maiores detalhes.

### 1. Função *Container* ([Superfícies](/fundamentos-visuais/superficie))

As *cores de superfície* são utilizadas como base cromática neutra para composição do tema e compõem harmonicamente com as demais cores. São usadas especificamente em planos de fundo nas telas da interface e superfícies de componentes. Também devem proporcionar contraste com o texto e elementos interativos, assim como qualquer outro elemento de comunicação visual na interface, como imagens, ilustrações, ícones, etc.

#### Especificação

 De modo geral, as cores de superfície mais utilizadas são as gradações de cinza, presentes na família `Gray` e branco ou preto na família `Pure`. Também pode se utilizar a família `Blue Warm Vivid` para fundos escuros. Elas são usadas como plano de fundo na tela e praticamente em todos os componentes do Design System.

##### Fundo Claro

| Legenda |  Recomendação   | Hexadecimal |   Token    |
| :-----: | :-------------: | :---------: | :--------: |
|  *P*  |  Cor Principal  |  `#ffffff`  | `--pure-0` |
|  *A*  | Cor Alternativa |  `#f8f8f8`  | `--gray-2` |

![Cor Superfície - Família Pure](imagens/pure-family.png)
*Cor Superfície - Família `Pure` para Fundos Claros*

![Cor Superfície Alternativa - Escala de Cinza](imagens/gray-superficie-light.png)
*Cor Superfície Alternativa - Família `Gray` para Fundos Claros*

##### Fundo Escuro

| Legenda | Recomendação  | Hexadecimal |         Token          |
| :-----: | :-----------: | :---------: | :--------------------: |
|  *P*  | Cor Principal |  `#001851`  | `--blue-warm-vivid-90` |
|  *A*  | Cor Alternativa |  `#002681`  | `--blue-warm-vivid-80` |

![Cor Superfície - Família Blue-Warm-Vivid](imagens/blue-superficie-dark.png)
*Cor Superfície - Família `Blue Warm Vivid` para Fundos Escuros*

### 2. Função Leitura ([Tipografia](/fundamentos-visuais/tipografia) e [Iconografia](/fundamentos-visuais/iconografia))

São as cores usadas nos elementos que necessitam de legibilidade, como textos e ícones, por exemplo. A função principal dessas cores é proporcionar contraste com a *Cor Superfície* usada na tela ou elementos. Logo, para toda *Cor Superfície*, existe uma *Cor Leitura* contrastante.

#### Especificação Leitura

##### Fundo Claro para Leitura

| Legenda | Recomendação  | Hexadecimal |    Token    |
| :-----: | :-----------: | :---------: | :---------: |
|  *P*  | Cor Principal |  `#333333`  | `--gray-80` |

![Cor Leitura - Família Gray](imagens/gray-reading-light.png)
*Cor Leitura - Família `Gray` para Fundos Claros*

##### Fundo Escuro para Leitura

| Legenda | Recomendação  | Hexadecimal |   Token    |
| :-----: | :-----------: | :---------: | :--------: |
|  *P*  | Cor Principal |  `#ffffff`  | `--pure-0` |

![Cor Leitura - Família Pure](imagens/pure-family.png)
*Cor Leitura - Família `Pure` para Fundos Escuros*

### 3. Função *Feedback* ([Estados](/fundamentos-visuais/estados))

São representações visuais para feedbacks interativos ou informacionais relacionadas aos elementos na interface. Podemos subdividir essa função em dois grupos de estados: *interação* e *avisos*.

#### Função Interação

São cores que servem para indicar ao usuário a possibilidade de interação sobre qualquer elemento que a utiliza. É necessário que estas cores se destaquem sobre os outros elementos ou informações textuais na interface.

##### Especificação Interação

Para este tipo de cor, utilizam-se as famílias `Blue Warm` e `Blue Warm Vivid`. Abaixo seguem as referências.

###### Fundo Claro para Interação

| Legenda | Recomendação  | Hexadecimal |         Token          |
| :-----: | :-----------: | :---------: | :--------------------: |
|  *P*  | Cor Principal |  `#003DC7`  | `--blue-warm-vivid-70` |

![Cor Interativa - Família Blue Warm Vivid](imagens/blue-warm-vivid-interactive-light.png)
*Cor Interativa - Família `Blue Warm Vivid` para Fundos Claros*

###### Fundo Escuro para Interação

| Legenda | Recomendação  | Hexadecimal |      Token       |
| :-----: | :-----------: | :---------: | :--------------: |
|  *P*  | Cor Principal |  `#A3BEFF`  | `--blue-warm-20` |

![Cor Interativa - Família Blue Warm](imagens/blue-warm-interactive-dark.png)
*Cor Interativa - Família `Blue Warm` para Fundos Escuros*

#### Função de Avisos

 As cores de avisos foram escolhidas para serem aplicadas em situações que exijam mais atenção do usuário, como ocorre nas mensagens de *Feedback* e estados correlacionados.

##### Especificação Avisos

São cores fortemente reconhecidas pela boa parte dos usuários e relacionadas a avisos específicos. Consulte o documento [Fundamentos Visuais > Estados](/fundamentos-visuais/estados) para maiores informações.

###### Feedback Informação

| Legenda |  Recomendação   | Hexadecimal |         Token          |
| :-----: | :-------------: | :---------: | :--------------------: |
|  *P*  |  Cor Principal  |  `#0043DE`  | `--blue-warm-vivid-60` |
|  *A*  | Cor Alternativa |  `#C3D5FF`  | `--blue-warm-vivid-10` |

![Cor Feedback Informação - Família Blue Warm Vivid](imagens/feedback-blue-warm-vivid.png)
*Cor Feedback Informação - Família `Blue Warm Vivid`*

###### Feedback Sucesso

| Legenda |  Recomendação   | Hexadecimal |          Token          |
| :-----: | :-------------: | :---------: | :---------------------: |
|  *P*  |  Cor Principal  |  `#008511`  | `--green-vivid-50` |
|  *A*  | Cor Alternativa |  `#AEFFC9`  | `--green-vivid-5`  |

![Cor Feedback Sucesso - Família Green Cool Vivid](imagens/feedback-green-vivid.png)
*Cor Feedback Sucesso - Família `Green Cool Vivid`*

###### Feedback Alerta

| Legenda |  Recomendação   | Hexadecimal |        Token        |
| :-----: | :-------------: | :---------: | :-----------------: |
|  *P*  |  Cor Principal  |  `#FFE10E`  | `--yellow-vivid-20` |
|  *A*  | Cor Alternativa |  `#FFFAC4`  | `--yellow-vivid-5`  |

![Cor Feedback Alerta - Família Yellow Vivid](imagens/feedback-yellow-vivid.png)
*Cor Feedback Alerta - Família `Yellow Vivid`*

###### Feedback Erro

| Legenda |  Recomendação   | Hexadecimal |      Token       |
| :-----: | :-------------: | :---------: | :--------------: |
|  *P*  |  Cor Principal  |  `#EE1400`  | `--red-vivid-50` |
|  *A*  | Cor Alternativa |  `#FFE3DA`  | `--red-vivid-10` |

![Cor Feedback Erro - Família Red Vivid](imagens/feedback-red-vivid.png)
*Cor Feedback Erro - Família `Red Vivid`*

 ---

## Paleta do Design System

Como dito anteriormente, a paleta do Design System foi constituída por grupos de famílias, formadas por grupos de cores dentro do círculo cromático (HSL), com um matiz igual ou muito próximo. As famílias alternativas estão descritas com sufixo `Vivid` junto ao nome da família.

Os nomes das cores são formados pelo nome da família correspondente, adicionando o fator de luminância ao qual ela pertence, dentro daquela família.

Abaixo estão listadas todas as famílias do Design System.

### Red

|              Cor               | Nome da Cor | Luminância |       HSL / Hexadecimal        |   Token    |
| :----------------------------: | :---------: | :--------: | :----------------------------: | :--------: |
| ![Paletas](imagens/231B1A.png) |  `Red 90`   |  `0.008`   | `hsl(7, 15%, 12%)` / `#231B1A` | `--red-90` |
| ![Paletas](imagens/4B2C25.png) |  `Red 80`   |  `0.027`   | `hsl(11, 34%, 22%)` / `#4B2C25` | `--red-80` |
| ![Paletas](imagens/892C20.png) |  `Red 70`   |  `0.059`   | `hsl(7, 62%, 33%)` / `#892C20` | `--red-70` |
| ![Paletas](imagens/CC2415.png) |  `Red 60`   |  `0.106`   | `hsl(5, 81%, 44%)` / `#CC2415` | `--red-60` |
| ![Paletas](imagens/FF2E13.png) |  `Red 50`   |  `0.177`   | `hsl(7, 100%, 54%)` / `#FF2E13` | `--red-50` |
| ![Paletas](imagens/FF6A4E.png) |  `Red 40`   |  `0.282`   | `hsl(9, 100%, 65%)` / `#FF6A4E` | `--red-40` |
| ![Paletas](imagens/FF9582.png) |  `Red 30`   |  `0.416`   | `hsl(9, 100%, 75%)` / `#FF9582` | `--red-30` |
| ![Paletas](imagens/FFBEAB.png) |  `Red 20`   |  `0.584`   | `hsl(14, 100%, 84%)` / `#FFBEAB` | `--red-20` |
| ![Paletas](imagens/FFE0D8.png) |  `Red 10`   |  `0.790`   | `hsl(12, 100%, 92%)` / `#FFE0D8` | `--red-10` |
| ![Paletas](imagens/FDECEA.png) |   `Red 5`   |  `0.874`   | `hsl(6, 83%, 95%)` / `#FDECEA` | `--red-5`  |

### Red Vivid

|              Cor               |  Nome da Cor   | Luminância |        HSL / Hexadecimal        |      Token       |
| :----------------------------: | :------------: | :--------: | :-----------------------------: | :--------------: |
| ![Paletas](imagens/780A00.png) | `Red Vivid 80` |  `0.027`   | `hsl(0, 69%, 21%)` / `#780A00`  | `--red-vivid-80` |
| ![Paletas](imagens/981500.png) | `Red Vivid 70` |  `0.057`   | `hsl(8, 100%, 30%)` / `#981500`  | `--red-vivid-70` |
| ![Paletas](imagens/C71000.png) | `Red Vivid 60` |  `0.100`   | `hsl(5, 100%, 39%)` / `#C71000`  | `--red-vivid-60` |
| ![Paletas](imagens/EE1400.png) | `Red Vivid 50` |  `0.178`   | `hsl(5, 100%, 47%)` / `#EE1400`  | `--red-vivid-50` |
| ![Paletas](imagens/FF6B48.png) | `Red Vivid 40` |  `0.282`   | `hsl(11, 100%, 64%)` / `#FF6B48`  | `--red-vivid-40` |
| ![Paletas](imagens/FF9A7F.png) | `Red Vivid 30` |  `0.417`   | `hsl(13, 100%, 75%)` / `#FF9A7F` | `--red-vivid-30` |
| ![Paletas](imagens/FFBFAE.png) | `Red Vivid 20` |  `0.582`   | `hsl(13, 100%, 84%)` / `#FFBFAE`  | `--red-vivid-20` |
| ![Paletas](imagens/FFE3DA.png) | `Red Vivid 10` |  `0.793`   | `hsl(15, 100%, 93%)` / `#FFE3DA`  | `--red-vivid-10` |
| ![Paletas](imagens/FFF4F2.png) | `Red Vivid 5`  |  `0.917`   | `hsl(9, 100%, 97%)` / `#FFF4F2` | `--red-vivid-5`  |

### Red Cool

|              Cor               |  Nome da Cor  | Luminância |        HSL / Hexadecimal         |      Token      |
| :----------------------------: | :-----------: | :--------: | :------------------------------: | :-------------: |
| ![Paletas](imagens/271719.png) | `Red Cool 90` |  `0.008`   | `hsl(353, 26%, 12%)` / `#271719` | `--red-cool-90` |
| ![Paletas](imagens/4D2529.png) | `Red Cool 80` |  `0.027`   | `hsl(354, 35%, 22%)` / `#4D2529` | `--red-cool-80` |
| ![Paletas](imagens/7E2A31.png) | `Red Cool 70` |  `0.059`   | `hsl(355, 50%, 33%)` / `#7E2A31` | `--red-cool-70` |
| ![Paletas](imagens/C6192A.png) | `Red Cool 60` |  `0.107`   | `hsl(354, 78%, 44%)` / `#C6192A` | `--red-cool-60` |
| ![Paletas](imagens/FF162E.png) | `Red Cool 50` |  `0.176`   | `hsl(354, 100%, 54%)` / `#FF162E` | `--red-cool-50` |
| ![Paletas](imagens/FF5262.png) | `Red Cool 40` |  `0.280`   | `hsl(354, 100%, 66%)` / `#FF5262` | `--red-cool-40` |
| ![Paletas](imagens/FA838C.png) | `Red Cool 30` |  `0.417`   | `hsl(355, 92%, 75%)` / `#FA838C` | `--red-cool-30` |
| ![Paletas](imagens/FEAFB6.png) | `Red Cool 20` |  `0.587`   | `hsl(355, 98%, 84%)` / `#FEAFB6` | `--red-cool-20` |
| ![Paletas](imagens/F9DBDE.png) | `Red Cool 10` |  `0.785`   | `hsl(354, 71%, 92%)` / `#F9DBDE` | `--red-cool-10` |
| ![Paletas](imagens/FBEBED.png) | `Red Cool 5`  |  `0.880`   | `hsl(352, 67%, 95%)` / `#FBEBED` | `--red-cool-5`  |

### Red Cool Vivid

|              Cor               |     Nome da Cor     | Luminância |         HSL / Hexadecimal         |         Token         |
| :----------------------------: | :-----------------: | :--------: | :-------------------------------: | :-------------------: |
| ![Paletas](imagens/660F16.png) | `Red Cool Vivid 80` |  `0.026`   | `hsl(355, 74%, 23%)` / `#660F16`  | `--red-cool-vivid-80` |
| ![Paletas](imagens/A80416.png) | `Red Cool Vivid 70` |  `0.060`   | `hsl(349, 60%, 32%` / `#A80416`  | `--red-cool-vivid-70` |
| ![Paletas](imagens/D70015.png) | `Red Cool Vivid 60` |  `0.106`   | `hsl(354, 100%, 42%)` / `#D70015`  | `--red-cool-vivid-60` |
| ![Paletas](imagens/FF091C.png) | `Red Cool Vivid 50` |  `0.177`   | `hsl(355, 100%, 52%)` / `#FF091C`  | `--red-cool-vivid-50` |
| ![Paletas](imagens/FF5668.png) | `Red Cool Vivid 40` |  `0.284`   | `hsl(354, 100%, 67%)` / `#FF5668`  | `--red-cool-vivid-40` |
| ![Paletas](imagens/FF8C98.png) | `Red Cool Vivid 30` |  `0.418`   | `hsl(354, 100%, 77%)` / `#FF8C98`  | `--red-cool-vivid-30` |
| ![Paletas](imagens/FFB4BC.png) | `Red Cool Vivid 20` |  `0.586`   | `hsl(349, 82%, 85%)` / `#FFB4BC`  | `--red-cool-vivid-20` |
| ![Paletas](imagens/FFD9DB.png) | `Red Cool Vivid 10` |  `0.782`   | `hsl(357, 100%, 93%)` / `#FFD9DB`  | `--red-cool-vivid-10` |
| ![Paletas](imagens/FFF2F4.png) | `Red Cool Vivid 5`  |  `0.913`   | `hsl(351, 100%, 97%)` / `#FFF2F4` | `--red-cool-vivid-5`  |

### Red Warm

|              Cor               |  Nome da Cor  | Luminância |        HSL / Hexadecimal        |      Token      |
| :----------------------------: | :-----------: | :--------: | :-----------------------------: | :-------------: |
| ![Paletas](imagens/27231B.png) | `Red Warm 90` |  `0.011`   | `hsl(40, 18%, 13%)` / `#27231B` | `--red-warm-90` |
| ![Paletas](imagens/3C332B.png) | `Red Warm 80` |  `0.027`   | `hsl(28, 17%, 20%)` / `#3C332B` | `--red-warm-80` |
| ![Paletas](imagens/614932.png) | `Red Warm 70` |  `0.059`   | `hsl(29, 32%, 29%)` / `#614932` | `--red-warm-70` |
| ![Paletas](imagens/9C5624.png) | `Red Warm 60` |  `0.106`   | `hsl(25, 63%, 38%)` / `#9C5624` | `--red-warm-60` |
| ![Paletas](imagens/F75000.png) | `Red Warm 50` |  `0.176`   | `hsl(19, 100%, 48%)` / `#F75000` | `--red-warm-50` |
| ![Paletas](imagens/FF7C2F.png) | `Red Warm 40` |  `0.282`   | `hsl(22, 100%, 59%)` / `#FF7C2F` | `--red-warm-40` |
| ![Paletas](imagens/FEA663.png) | `Red Warm 30` |  `0.419`   | `hsl(26, 99%, 69%` / `#FEA663` | `--red-warm-30` |
| ![Paletas](imagens/FFC697.png) | `Red Warm 20` |  `0.583`   | `hsl(27, 100%, 80%)` / `#FFC697` | `--red-warm-20` |
| ![Paletas](imagens/FDE4D3.png) | `Red Warm 10` |  `0.792`   | `hsl(24, 91%, 91%)` / `#FDE4D3` | `--red-warm-10` |
| ![Paletas](imagens/FAEFE7.png) | `Red Warm 5`  |  `0.872`   | `hsl(25, 66%, 94%)` / `#FAEFE7` | `--red-warm-5`  |

### Red Warm Vivid

|              Cor               |     Nome da Cor     | Luminância |        HSL / Hexadecimal         |         Token         |
| :----------------------------: | :-----------------: | :--------: | :------------------------------: | :-------------------: |
| ![Paletas](imagens/4E3118.png) | `Red Warm Vivid 80` |  `0.027`   | `hsl(28, 53%, 20%)` / `#4E3118`  | `--red-warm-vivid-80` |
| ![Paletas](imagens/7D4100.png) | `Red Warm Vivid 70` |  `0.051`   | `hsl(31, 100%, 25%)` / `#7D4100`  | `--red-warm-vivid-70` |
| ![Paletas](imagens/B64900.png) | `Red Warm Vivid 60` |  `0.104`   | `hsl(24, 100%, 36%)` / `#B64900`  | `--red-warm-vivid-60` |
| ![Paletas](imagens/E65400.png) | `Red Warm Vivid 50` |  `0.181`   | `hsl(22, 100%, 45%)` / `#E65400`  | `--red-warm-vivid-50` |
| ![Paletas](imagens/FF6E1B.png) | `Red Warm Vivid 40` |  `0.264`   | `hsl(22, 100%, 55%)` / `#FF6E1B`  | `--red-warm-vivid-40` |
| ![Paletas](imagens/FF9D60.png) | `Red Warm Vivid 30` |  `0.406`   | `hsl(23, 100%, 69%)` / `#FF9D60`  | `--red-warm-vivid-30` |
| ![Paletas](imagens/FFC596.png) | `Red Warm Vivid 20` |  `0.583`   | `hsl(27, 100%, 79%)` / `#FFC596`  | `--red-warm-vivid-20` |
| ![Paletas](imagens/FFE5D2.png) | `Red Warm Vivid 10` |  `0.792`   | `hsl(25, 100%, 91%)` / `#FFE5D2`  | `--red-warm-vivid-10` |
| ![Paletas](imagens/FFF6EE.png) | `Red Warm Vivid 5`  |  `0.927`   | `hsl(28, 100%, 97%)` / `#FFF6EE` | `--red-warm-vivid-5`  |

### Orange

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal        |     Token     |
| :----------------------------: | :---------: | :--------: | :-----------------------------: | :-----------: |
| ![Paletas](imagens/231B17.png) | `Orange 90` |  `0.008`   | `hsl(20, 21%, 11%)` / `#231B17`  | `--orange-90` |
| ![Paletas](imagens/3D3428.png) | `Orange 80` |  `0.027`   | `hsl(34, 21%, 20%)` / `#3D3428` | `--orange-80` |
| ![Paletas](imagens/614932.png) | `Orange 70` |  `0.059`   | `hsl(29, 32%, 29%)` / `#614932` | `--orange-70` |
| ![Paletas](imagens/8F5D31.png) | `Orange 60` |  `0.107`   | `hsl(28, 49%, 38%)` / `#8F5D31` | `--orange-60` |
| ![Paletas](imagens/D36F13.png) | `Orange 50` |  `0.176`   | `hsl(29, 83%, 45%)` / `#D36F13` | `--orange-50` |
| ![Paletas](imagens/FF8418.png) | `Orange 40` |  `0.283`   | `hsl(28, 100%, 55%)` / `#FF8418` | `--orange-40` |
| ![Paletas](imagens/FFA555.png) | `Orange 30` |  `0.418`   | `hsl(28, 100%, 67%)` / `#FFA555` | `--orange-30` |
| ![Paletas](imagens/FFCA87.png) | `Orange 20` |  `0.583`   | `hsl(33, 100%, 76%)` / `#FFCA87` | `--orange-20` |
| ![Paletas](imagens/FDEACA.png) | `Orange 10` |  `0.791`   | `hsl(38, 93%, 89%)` / `#FDEACA` | `--orange-10` |
| ![Paletas](imagens/FAEFE5.png) | `Orange 5`  |  `0.872`   | `hsl(29, 68%, 94%)` / `#FAEFE5` | `--orange-5`  |

### Orange Vivid

|              Cor               |    Nome da Cor    | Luminância |        HSL / Hexadecimal         |        Token        |
| :----------------------------: | :---------------: | :--------: | :------------------------------: | :-----------------: |
| ![Paletas](imagens/472C0C.png) | `Orange Vivid 80` |  `0.020`   | `hsl(33, 71%, 16%)` / `#472C0C`  | `--orange-vivid-80` |
| ![Paletas](imagens/7D4303.png) | `Orange Vivid 70` |  `0.051`   | `hsl(31, 95%, 25%)` / `#7D4303`  | `--orange-vivid-70` |
| ![Paletas](imagens/B15200.png) | `Orange Vivid 60` |  `0.101`   | `hsl(28, 100%, 35%)` / `#B15200`  | `--orange-vivid-60` |
| ![Paletas](imagens/C96B00.png) | `Orange Vivid 50` |  `0.178`   | `hsl(32, 100%, 39%)` / `#C96B00` | `--orange-vivid-50` |
| ![Paletas](imagens/FC8500.png) | `Orange Vivid 40` |  `0.282`   | `hsl(32, 100%, 49%)` / `#FC8500`  | `--orange-vivid-40` |
| ![Paletas](imagens/FFA641.png) | `Orange Vivid 30` |  `0.400`   | `hsl(32, 100%, 63%)` / `#FFA641` | `--orange-vivid-30` |
| ![Paletas](imagens/FFC87C.png) | `Orange Vivid 20` |  `0.585`   | `hsl(35, 100%, 74%)` / `#FFC87C` | `--orange-vivid-20` |
| ![Paletas](imagens/FFE8C4.png) | `Orange Vivid 10` |  `0.791`   | `hsl(37, 100%, 88%)` / `#FFE8C4`  | `--orange-vivid-10` |
| ![Paletas](imagens/FFF4E4.png) | `Orange Vivid 5`  |  `0.919`   | `hsl(36, 100%, 95%)` / `#FFF4E4`  | `--orange-vivid-5`  |

### Orange Warm

|              Cor               |   Nome da Cor    | Luminância |        HSL / Hexadecimal        |       Token        |
| :----------------------------: | :--------------: | :--------: | :-----------------------------: | :----------------: |
| ![Paletas](imagens/241B18.png) | `Orange Warm 90` |  `0.008`   | `hsl(15, 20%, 12%)` / `#241B18`  | `--orange-warm-90` |
| ![Paletas](imagens/4A2C22.png) | `Orange Warm 80` |  `0.027`   | `hsl(15, 37%, 21%)` / `#4A2C22` | `--orange-warm-80` |
| ![Paletas](imagens/783926.png) | `Orange Warm 70` |  `0.059`   | `hsl(14, 52%, 31%)` / `#783926` | `--orange-warm-70` |
| ![Paletas](imagens/B54418.png) | `Orange Warm 60` |  `0.107`   | `hsl(17, 77%, 40%)` / `#B54418` | `--orange-warm-60` |
| ![Paletas](imagens/EC5F00.png) | `Orange Warm 50` |  `0.177`   | `hsl(24, 100%, 46%)` / `#EC5F00` | `--orange-warm-50` |
| ![Paletas](imagens/FF7B29.png) | `Orange Warm 40` |  `0.281`   | `hsl(23, 100%, 58%)` / `#FF7B29` | `--orange-warm-40` |
| ![Paletas](imagens/FFA165.png) | `Orange Warm 30` |  `0.419`   | `hsl(23, 100%, 70%)` / `#FFA165` | `--orange-warm-30` |
| ![Paletas](imagens/FFC39D.png) | `Orange Warm 20` |  `0.583`   | `hsl(23, 100%, 81%)` / `#FFC39D` | `--orange-warm-20` |
| ![Paletas](imagens/FFE4CD.png) | `Orange Warm 10` |  `0.783`   | `hsl(28, 100%, 90%)` / `#FFE4CD` | `--orange-warm-10` |
| ![Paletas](imagens/FFEFE1.png) | `Orange Warm 5`  |  `0.871`   | `hsl(28, 100%, 94%)` / `#FFEFE1` | `--orange-warm-5`  |

### Orange Warm Vivid

|              Cor               |      Nome da Cor       | Luminância |        HSL / Hexadecimal         |          Token           |
| :----------------------------: | :--------------------: | :--------: | :------------------------------: | :----------------------: |
| ![Paletas](imagens/4D2617.png) | `Orange Warm Vivid 80` |  `0.022`   | `hsl(17, 54%, 20%)` / `#4D2617`  | `--orange-warm-vivid-80` |
| ![Paletas](imagens/952400.png) | `Orange Warm Vivid 70` |  `0.052`   | `hsl(14, 100%, 29%)` / `#952400`  | `--orange-warm-vivid-70` |
| ![Paletas](imagens/C03700.png) | `Orange Warm Vivid 60` |  `0.102`   | `hsl(17, 100%, 38%)` / `#C03700`  | `--orange-warm-vivid-60` |
| ![Paletas](imagens/D75D00.png) | `Orange Warm Vivid 50` |  `0.180`   | `hsl(26, 100%, 42%)` / `#D75D00` | `--orange-warm-vivid-50` |
| ![Paletas](imagens/FF7011.png) | `Orange Warm Vivid 40` |  `0.282`   | `hsl(24, 100%, 53%)` / `#FF7011` | `--orange-warm-vivid-40` |
| ![Paletas](imagens/FF9D6E.png) | `Orange Warm Vivid 30` |  `0.417`   | `hsl(19, 100%, 72%)` / `#FF9D6E`  | `--orange-warm-vivid-30` |
| ![Paletas](imagens/FFC1A5.png) | `Orange Warm Vivid 20` |  `0.584`   | `hsl(19, 100%, 82%)` / `#FFC1A5`  | `--orange-warm-vivid-20` |
| ![Paletas](imagens/FFE7D2.png) | `Orange Warm Vivid 10` |  `0.802`   | `hsl(28, 100%, 91%)` / `#FFE7D2` | `--orange-warm-vivid-10` |
| ![Paletas](imagens/FFF5EB.png) | `Orange Warm Vivid 5`  |  `0.913`   | `hsl(30, 100%, 96%)` / `#FFF5EB` | `--orange-warm-vivid-5`  |

### Gold

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal        |    Token    |
| :----------------------------: | :---------: | :--------: | :-----------------------------: | :---------: |
| ![Paletas](imagens/211E18.png) |  `Gold 90`  |  `0.008`   | `hsl(40, 16%, 11%)` / `#211E18`  | `--gold-90` |
| ![Paletas](imagens/3C3527.png) |  `Gold 80`  |  `0.026`   | `hsl(40, 21%, 19%)` / `#3C3527` | `--gold-80` |
| ![Paletas](imagens/5A4D35.png) |  `Gold 70`  |  `0.059`   | `hsl(39, 26%, 28%)` / `#5A4D35` | `--gold-70` |
| ![Paletas](imagens/7C633F.png) |  `Gold 60`  |  `0.107`   | `hsl(35, 33%, 37%)` / `#7C633F` | `--gold-60` |
| ![Paletas](imagens/A87E3D.png) |  `Gold 50`  |  `0.179`   | `hsl(36, 47%, 45%)` / `#A87E3D` | `--gold-50` |
| ![Paletas](imagens/C9994F.png) |  `Gold 40`  |  `0.282`   | `hsl(36, 53%, 55%)` / `#C9994F` | `--gold-40` |
| ![Paletas](imagens/E4BA63.png) |  `Gold 30`  |  `0.419`   | `hsl(40, 70%, 64%)` / `#E4BA63` | `--gold-30` |
| ![Paletas](imagens/F7D884.png) |  `Gold 20`  |  `0.582`   | `hsl(44, 88%, 74%)` / `#F7D884` | `--gold-20` |
| ![Paletas](imagens/FEF0C1.png) |  `Gold 10`  |  `0.791`   | `hsl(46, 97%, 88%)` / `#FEF0C1` | `--gold-10` |
| ![Paletas](imagens/FAF3E2.png) |  `Gold 5`   |  `0.874`   | `hsl(42, 71%, 93%)` / `#FAF3E2` | `--gold-5`  |

### Gold Vivid

|              Cor               |   Nome da Cor   | Luminância |        HSL / Hexadecimal         |       Token       |
| :----------------------------: | :-------------: | :--------: | :------------------------------: | :---------------: |
| ![Paletas](imagens/4E390D.png) | `Gold Vivid 80` |  `0.027`   | `hsl(41, 71%, 18%)` / `#4E390D`  | `--gold-vivid-80` |
| ![Paletas](imagens/715500.png) | `Gold Vivid 70` |  `0.060`   | `hsl(45, 100%, 22%)` / `#715500`  | `--gold-vivid-70` |
| ![Paletas](imagens/9E7500.png) | `Gold Vivid 60` |  `0.113`   | `hsl(44, 100%, 31%)` / `#9E7500`  | `--gold-vivid-60` |
| ![Paletas](imagens/B7871C.png) | `Gold Vivid 50` |  `0.178`   | `hsl(41, 73%, 41%)` / `#B7871C`  | `--gold-vivid-50` |
| ![Paletas](imagens/D7A000.png) | `Gold Vivid 40` |  `0.282`   | `hsl(45, 100%, 42%)` / `#D7A000`  | `--gold-vivid-40` |
| ![Paletas](imagens/EDB800.png) | `Gold Vivid 30` |  `0.417`   | `hsl(47, 100%, 46%)` / `#EDB800` | `--gold-vivid-30` |
| ![Paletas](imagens/FFD034.png) | `Gold Vivid 20` |  `0.582`   | `hsl(46, 100%, 60%)` / `#FFD034` | `--gold-vivid-20` |
| ![Paletas](imagens/FFEC99.png) | `Gold Vivid 10` |  `0.784`   | `hsl(44, 100%, 79%)` / `#FFEC99` | `--gold-vivid-10` |
| ![Paletas](imagens/FFF5C9.png) | `Gold Vivid 5`  |  `0.875`   | `hsl(49, 100%, 89%)` / `#FFF5C9`  | `--gold-vivid-5`  |

### Yellow

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal        |     Token     |
| :----------------------------: | :---------: | :--------: | :-----------------------------: | :-----------: |
| ![Paletas](imagens/221C18.png) | `Yellow 90` |  `0.008`   | `hsl(24, 17%, 11%)` / `#221C18`  | `--yellow-90` |
| ![Paletas](imagens/3D3428.png) | `Yellow 80` |  `0.027`   | `hsl(34, 21%, 20%)` / `#3D3428` | `--yellow-80` |
| ![Paletas](imagens/604D2D.png) | `Yellow 70` |  `0.059`   | `hsl(38, 36%, 28%)` / `#604D2D` | `--yellow-70` |
| ![Paletas](imagens/816C2B.png) | `Yellow 60` |  `0.107`   | `hsl(45, 50%, 34%)` / `#816C2B` | `--yellow-60` |
| ![Paletas](imagens/AB8E1E.png) | `Yellow 50` |  `0.177`   | `hsl(48, 70%, 39%)` / `#AB8E1E` | `--yellow-50` |
| ![Paletas](imagens/CCAF2A.png) | `Yellow 40` |  `0.284`   | `hsl(49, 66%, 48%)` / `#CCAF2A` | `--yellow-40` |
| ![Paletas](imagens/F8D71E.png) | `Yellow 30` |  `0.420`   | `hsl(51, 94%, 55%)` / `#F8D71E` | `--yellow-30` |
| ![Paletas](imagens/FFE739.png) | `Yellow 20` |  `0.581`   | `hsl(53, 100%, 61%)` / `#FFE739` | `--yellow-20` |
| ![Paletas](imagens/FFF4A7.png) | `Yellow 10` |  `0.791`   | `hsl(53, 100%, 83%)` / `#FFF4A7` | `--yellow-10` |
| ![Paletas](imagens/FFFACD.png) | `Yellow 5`  |  `0.890`   | `hsl(54, 100%, 90%)` / `#FFFACD` | `--yellow-5`  |

### Yellow Vivid

|              Cor               |    Nome da Cor    | Luminância |        HSL / Hexadecimal         |        Token        |
| :----------------------------: | :---------------: | :--------: | :------------------------------: | :-----------------: |
| ![Paletas](imagens/563910.png) | `Yellow Vivid 80` |  `0.031`   | `hsl(35, 69%, 20%)` / `#563910`  | `--yellow-vivid-80` |
| ![Paletas](imagens/705E00.png) | `Yellow Vivid 70` |  `0.069`   | `hsl(50, 100%, 22%)` / `#705E00`  | `--yellow-vivid-70` |
| ![Paletas](imagens/987F00.png) | `Yellow Vivid 60` |  `0.123`   | `hsl(50, 100%, 30%)` / `#987F00`  | `--yellow-vivid-60` |
| ![Paletas](imagens/9E8500.png) | `Yellow Vivid 50` |  `0.181`   | `hsl(51, 100%, 31%)` / `#9E8500` | `--yellow-vivid-50` |
| ![Paletas](imagens/BCA200.png) | `Yellow Vivid 40` |  `0.283`   | `hsl(52, 100%, 37%)` / `#BCA200` | `--yellow-vivid-40` |
| ![Paletas](imagens/E6C200.png) | `Yellow Vivid 30` |  `0.441`   | `hsl(51, 100%, 45%)` / `#E6C200`  | `--yellow-vivid-30` |
| ![Paletas](imagens/FFE10E.png) | `Yellow Vivid 20` |  `0.649`   | `hsl(53, 100%, 53%)` / `#FFE10E` | `--yellow-vivid-20` |
| ![Paletas](imagens/FFF187.png) | `Yellow Vivid 10` |  `0.793`   | `hsl(53, 100%, 76%)` / `#FFF187`  | `--yellow-vivid-10` |
| ![Paletas](imagens/FFFAC4.png) | `Yellow Vivid 5`  |  `0.904`   | `hsl(55, 100%, 88%)` / `#FFFAC4` | `--yellow-vivid-5`  |

### Green

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal        |    Token     |
| :----------------------------: | :---------: | :--------: | :-----------------------------: | :----------: |
| ![Paletas](imagens/151F18.png) | `Green 90`  |  `0.008`   |  `hsl(138, 19%, 10%)` / `#151F18`  | `--green-90` |
| ![Paletas](imagens/193E21.png) | `Green 80`  |  `0.026`   | `hsl(133, 43%, 17%)` / `#193E21` | `--green-80` |
| ![Paletas](imagens/146324.png) | `Green 70`  |  `0.060`   | `hsl(132, 66%, 23%)` / `#146324` | `--green-70` |
| ![Paletas](imagens/008B13.png) | `Green 60`  |  `0.107`   | `hsl(128, 100%, 27%)` / `#008B13` | `--green-60` |
| ![Paletas](imagens/02B325.png) | `Green 50`  |  `0.179`   | `hsl(132, 98%, 35%)` / `#02B325` | `--green-50` |
| ![Paletas](imagens/17CE33.png) | `Green 40`  |  `0.283`   | `hsl(129, 80%, 45%)` / `#17CE33` | `--green-40` |
| ![Paletas](imagens/3FDF59.png) | `Green 30`  |  `0.416`   | `hsl(130, 71%, 56%)` / `#3FDF59` | `--green-30` |
| ![Paletas](imagens/61F67C.png) | `Green 20`  |  `0.583`   | `hsl(131, 89%, 67%)` / `#61F67C` | `--green-20` |
| ![Paletas](imagens/AFF5B9.png) | `Green 10`  |  `0.789`   | `hsl(129, 78%, 82%)` / `#AFF5B9` | `--green-10` |
| ![Paletas](imagens/C3F8CD.png) |  `Green 5`  |  `0.874`   | `hsl(131, 79%, 87%)` / `#C3F8CD` | `--green-5`  |

### Green Vivid

|              Cor               |   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :----------------------------: | :--------------: | :--------: | :------------------------------: | :----------------: |
| ![Paletas](imagens/004E14.png) | `Green Vivid 80` |  `0.028`   | `hsl(135, 100%, 15%)` / `#004E14`  | `--green-vivid-80` |
| ![Paletas](imagens/005C12.png) | `Green Vivid 70` |  `0.055`   | `hsl(132, 100%, 18%)` / `#005C12`  | `--green-vivid-70` |
| ![Paletas](imagens/00750F.png) | `Green Vivid 60` |  `0.120`   | `hsl(128, 100%, 23%)` / `#00750F`  | `--green-vivid-60` |
| ![Paletas](imagens/008511.png) | `Green Vivid 50` |  `0.178`   | `hsl(128, 100%, 26%)` / `#008511` | `--green-vivid-50` |
| ![Paletas](imagens/00C721.png) | `Green Vivid 40` |  `0.284`   | `hsl(130, 100%, 39%)` / `#00C721`  | `--green-vivid-40` |
| ![Paletas](imagens/00E226.png) | `Green Vivid 30` |  `0.362`   | `hsl(130, 100%, 44%)` / `#00E226`  | `--green-vivid-30` |
| ![Paletas](imagens/00FF21.png) | `Green Vivid 20` |  `0.520`   | `hsl(128, 100%, 50%)` / `#00FF21`  | `--green-vivid-20` |
| ![Paletas](imagens/73FF90.png) | `Green Vivid 10` |  `0.751`   | `hsl(132, 100%, 73%)` / `#73FF90`  | `--green-vivid-10` |
| ![Paletas](imagens/AEFFC9.png) | `Green Vivid 5`  |  `0.872`   | `hsl(140, 100%, 84%)` / `#AEFFC9`  | `--green-vivid-5`  |

### Green Cool

|              Cor               |   Nome da Cor   | Luminância |        HSL / Hexadecimal         |       Token       |
| :----------------------------: | :-------------: | :--------: | :------------------------------: | :---------------: |
| ![Paletas](imagens/1B2725.png) | `Green Cool 90` |  `0.012`   | `hsl(170, 18%, 13%)` / `#1B2725`  | `--green-cool-90` |
| ![Paletas](imagens/243A3A.png) | `Green Cool 80` |  `0.028`   | `hsl(180, 23%, 18%)` / `#243A3A` | `--green-cool-80` |
| ![Paletas](imagens/2C5654.png) | `Green Cool 70` |  `0.058`   | `hsl(177, 32%, 25%)` / `#2C5654` | `--green-cool-70` |
| ![Paletas](imagens/2C7B66.png) | `Green Cool 60` |  `0.107`   | `hsl(164, 47%, 33%)` / `#2C7B66` | `--green-cool-60` |
| ![Paletas](imagens/29A198.png) | `Green Cool 50` |  `0.176`   | `hsl(176, 59%, 40%)` / `#29A198` | `--green-cool-50` |
| ![Paletas](imagens/2EC9BD.png) | `Green Cool 40` |  `0.281`   | `hsl(175, 63%, 48%)` / `#2EC9BD` | `--green-cool-40` |
| ![Paletas](imagens/5CD6CC.png) | `Green Cool 30` |  `0.417`   | `hsl(175, 60%, 60%)` / `#5CD6CC` | `--green-cool-30` |
| ![Paletas](imagens/99DBD6.png) | `Green Cool 20` |  `0.583`   | `hsl(175, 48%, 73%)` / `#99DBD6` | `--green-cool-20` |
| ![Paletas](imagens/C5EDEC.png) | `Green Cool 10` |  `0.797`   | `hsl(179, 53%, 85%)` / `#C5EDEC` | `--green-cool-10` |
| ![Paletas](imagens/DEEBE8.png) | `Green Cool 5`  |  `0.879`   | `hsl(166, 25%, 90%)` / `#DEEBE8` | `--green-cool-5`  |

### Green Cool Vivid

|              Cor               |      Nome da Cor      | Luminância |         HSL / Hexadecimal         |          Token          |
| :----------------------------: | :-------------------: | :--------: | :-------------------------------: | :---------------------: |
| ![Paletas](imagens/0B4643.png) | `Green Cool Vivid 80` |  `0.024`   | `hsl(177, 73%, 16%)` / `#0B4643`  | `--green-cool-vivid-80` |
| ![Paletas](imagens/006765.png) | `Green Cool Vivid 70` |  `0.054`   | `hsl(179, 100%, 20%)` / `#006765`  | `--green-cool-vivid-70` |
| ![Paletas](imagens/008F68.png) | `Green Cool Vivid 60` |  `0.115`   | `hsl(164, 100%, 28%)` / `#008F68`  | `--green-cool-vivid-60` |
| ![Paletas](imagens/009F89.png) | `Green Cool Vivid 50` |  `0.178`   | `hsl(172, 100%, 31%)` / `#009F89`  | `--green-cool-vivid-50` |
| ![Paletas](imagens/00AA9D.png) | `Green Cool Vivid 40` |  `0.284`   | `hsl(175, 100%, 33%)` / `#00AA9D` | `--green-cool-vivid-40` |
| ![Paletas](imagens/00E6CA.png) | `Green Cool Vivid 30` |  `0.418`   | `hsl(173, 100%, 45%)` / `#00E6CA`  | `--green-cool-vivid-30` |
| ![Paletas](imagens/47FFE5.png) | `Green Cool Vivid 20` |  `0.587`   | `hsl(172, 100%, 64%)` / `#47FFE5`  | `--green-cool-vivid-20` |
| ![Paletas](imagens/9BFFF1.png) | `Green Cool Vivid 10` |  `0.790`   | `hsl(172, 100%, 80%)` / `#9BFFF1`  | `--green-cool-vivid-10` |
| ![Paletas](imagens/CAF7E7.png) | `Green Cool Vivid 5`  |  `0.870`   | `hsl(159, 74%, 88%)` / `#CAF7E7`  | `--green-cool-vivid-5`  |

### Green Warm

|              Cor               |   Nome da Cor   | Luminância |        HSL / Hexadecimal        |       Token       |
| :----------------------------: | :-------------: | :--------: | :-----------------------------: | :---------------: |
| ![Paletas](imagens/161E14.png) | `Green Warm 90` |  `0.008`   | `hsl(108, 20%, 10%)` / `#161E14`  | `--green-warm-90` |
| ![Paletas](imagens/1F3C1A.png) | `Green Warm 80` |  `0.027`   | `hsl(111, 40%, 17%)` / `#1F3C1A` | `--green-warm-80` |
| ![Paletas](imagens/285921.png) | `Green Warm 70` |  `0.059`   | `hsl(113, 46%, 24%)` / `#285921` | `--green-warm-70` |
| ![Paletas](imagens/287A1E.png) | `Green Warm 60` |  `0.106`   | `hsl(113, 61%, 30%)` / `#287A1E` | `--green-warm-60` |
| ![Paletas](imagens/1DA119.png) | `Green Warm 50` |  `0.176`   | `hsl(118, 73%, 36%)` / `#1DA119` | `--green-warm-50` |
| ![Paletas](imagens/1DCB15.png) | `Green Warm 40` |  `0.283`   | `hsl(117, 81%, 44%)` / `#1DCB15` | `--green-warm-40` |
| ![Paletas](imagens/22F213.png) | `Green Warm 30` |  `0.418`   | `hsl(116, 90%, 51%)` / `#22F213` | `--green-warm-30` |
| ![Paletas](imagens/5FFF41.png) | `Green Warm 20` |  `0.597`   | `hsl(111, 100%, 63%)` / `#5FFF41` | `--green-warm-20` |
| ![Paletas](imagens/A3FF90.png) | `Green Warm 10` |  `0.792`   | `hsl(110, 100%, 78%)` / `#A3FF90` | `--green-warm-10` |
| ![Paletas](imagens/C1FDB8.png) | `Green Warm 5`  |  `0.883`   | `hsl(112, 95%, 86%)` / `#C1FDB8` | `--green-warm-5`  |

### Green Warm Vivid

|              Cor               |      Nome da Cor      | Luminância |        HSL / Hexadecimal         |          Token          |
| :----------------------------: | :-------------------: | :--------: | :------------------------------: | :---------------------: |
| ![Paletas](imagens/124B00.png) | `Green Warm Vivid 80` |  `0.036`   | `hsl(106, 100%, 15%)` / `#124B00`  | `--green-warm-vivid-80` |
| ![Paletas](imagens/136400.png) | `Green Warm Vivid 70` |  `0.069`   | `hsl(109, 100%, 20%)` / `#136400`  | `--green-warm-vivid-70` |
| ![Paletas](imagens/0C7D00.png) | `Green Warm Vivid 60` |  `0.117`   | `hsl(114, 100%, 25%)` / `#0C7D00`  | `--green-warm-vivid-60` |
| ![Paletas](imagens/098200.png) | `Green Warm Vivid 50` |  `0.177`   | `hsl(116, 100%, 25%)` / `#098200` | `--green-warm-vivid-50` |
| ![Paletas](imagens/00B900.png) | `Green Warm Vivid 40` |  `0.283`   | `hsl(120, 100%, 36%)` / `#00B900`  | `--green-warm-vivid-40` |
| ![Paletas](imagens/14E000.png) | `Green Warm Vivid 30` |  `0.418`   | `hsl(115, 100%, 44%)` / `#14E000`  | `--green-warm-vivid-30` |
| ![Paletas](imagens/24DA00.png) | `Green Warm Vivid 20` |  `0.584`   | `hsl(110, 100%, 43%)` / `#24DA00`  | `--green-warm-vivid-20` |
| ![Paletas](imagens/45FF21.png) | `Green Warm Vivid 10` |  `0.819`   | `hsl(110, 100%, 56%)` / `#45FF21`  | `--green-warm-vivid-10` |
| ![Paletas](imagens/B6FFAA.png) | `Green Warm Vivid 5`  |  `0.922`   | `hsl(112, 100%, 83%)` / `#B6FFAA`  | `--green-warm-vivid-5`  |

### Mint

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :----------------------------: | :---------: | :--------: | :------------------------------: | :---------: |
| ![Paletas](imagens/0F2419.png) |  `Mint 90`  |  `0.008`   | `hsl(149, 41%, 10%)` / `#0F2419`  | `--mint-90` |
| ![Paletas](imagens/15422C.png) |  `Mint 80`  |  `0.027`   | `hsl(151, 52%, 17%)` / `#15422C` | `--mint-80` |
| ![Paletas](imagens/15643E.png) |  `Mint 70`  |  `0.060`   | `hsl(151, 65%, 24%)` / `#15643E` | `--mint-70` |
| ![Paletas](imagens/178352.png) |  `Mint 60`  |  `0.107`   | `hsl(153, 70%, 30%)` / `#178352` | `--mint-60` |
| ![Paletas](imagens/15A480.png) |  `Mint 50`  |  `0.177`   | `hsl(165, 77%, 36%)` / `#15A480` | `--mint-50` |
| ![Paletas](imagens/11CE9E.png) |  `Mint 40`  |  `0.284`   | `hsl(165, 85%, 44%)` / `#11CE9E` | `--mint-40` |
| ![Paletas](imagens/3AE5AB.png) |  `Mint 30`  |  `0.416`   | `hsl(160, 77%, 56%)` / `#3AE5AB` | `--mint-30` |
| ![Paletas](imagens/7BF3CA.png) |  `Mint 20`  |  `0.593`   | `hsl(160, 83%, 72%)` / `#7BF3CA` | `--mint-20` |
| ![Paletas](imagens/BBFDED.png) |  `Mint 10`  |  `0.793`   | `hsl(165, 94%, 86%)` / `#BBFDED` | `--mint-10` |
| ![Paletas](imagens/D3FFF3.png) |  `Mint 5`   |  `0.870`   | `hsl(164, 100%, 91%)` / `#D3FFF3` | `--mint-5`  |

### Mint Vivid

|              Cor               |   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :----------------------------: | :-------------: | :--------: | :-------------------------------: | :---------------: |
| ![Paletas](imagens/054926.png) | `Mint Vivid 80` |  `0.027`   | `hsl(149, 87%, 15%)` / `#054926`  | `--mint-vivid-80` |
| ![Paletas](imagens/006635.png) | `Mint Vivid 70` |  `0.056`   | `hsl(151, 100%, 20%)` / `#006635`  | `--mint-vivid-70` |
| ![Paletas](imagens/00875C.png) | `Mint Vivid 60` |  `0.107`   | `hsl(161, 100%, 26%)` / `#00875C`  | `--mint-vivid-60` |
| ![Paletas](imagens/00906C.png) | `Mint Vivid 50` |  `0.177`   | `hsl(165, 100%, 28%)` / `#00906C` | `--mint-vivid-50` |
| ![Paletas](imagens/00B185.png) | `Mint Vivid 40` |  `0.291`   | `hsl(165, 100%, 35%)` / `#00B185` | `--mint-vivid-40` |
| ![Paletas](imagens/00D29D.png) | `Mint Vivid 30` |  `0.416`   | `hsl(165, 100%, 41%)` / `#00D29D`  | `--mint-vivid-30` |
| ![Paletas](imagens/01FFC2.png) | `Mint Vivid 20` |  `0.633`   | `hsl(166, 100%, 50%)` / `#01FFC2`  | `--mint-vivid-20` |
| ![Paletas](imagens/83FFE0.png) | `Mint Vivid 10` |  `0.791`   | `hsl(165, 100%, 76%)` / `#83FFE0`  | `--mint-vivid-10` |
| ![Paletas](imagens/C6FFF2.png) | `Mint Vivid 5`  |  `0.874`   | `hsl(166, 100%, 89%)` / `#C6FFF2`  | `--mint-vivid-5`  |

### Mint Cool

|              Cor               |  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :----------------------------: | :------------: | :--------: | :------------------------------: | :--------------: |
| ![Paletas](imagens/161F20.png) | `Mint Cool 90` |  `0.008`   | `hsl(186, 19%, 11%)` / `#161F20`  | `--mint-cool-90` |
| ![Paletas](imagens/1F3A3D.png) | `Mint Cool 80` |  `0.027`   | `hsl(186, 33%, 18%)` / `#1F3A3D` | `--mint-cool-80` |
| ![Paletas](imagens/245C56.png) | `Mint Cool 70` |  `0.059`   | `hsl(174, 44%, 25%)` / `#245C56` | `--mint-cool-70` |
| ![Paletas](imagens/2B7578.png) | `Mint Cool 60` |  `0.108`   | `hsl(182, 47%, 32%)` / `#2B7578` | `--mint-cool-60` |
| ![Paletas](imagens/2E929A.png) | `Mint Cool 50` |  `0.180`   | `hsl(184, 54%, 39%)` / `#2E929A` | `--mint-cool-50` |
| ![Paletas](imagens/37BABD.png) | `Mint Cool 40` |  `0.284`   | `hsl(181, 55%, 48%)` / `#37BABD` | `--mint-cool-40` |
| ![Paletas](imagens/58D7D5.png) | `Mint Cool 30` |  `0.417`   | `hsl(179, 61%, 59%)` / `#58D7D5` | `--mint-cool-30` |
| ![Paletas](imagens/89E9E8.png) | `Mint Cool 20` |  `0.585`   | `hsl(179, 69%, 73%)` / `#89E9E8` | `--mint-cool-20` |
| ![Paletas](imagens/B7FBFD.png) | `Mint Cool 10` |  `0.788`   | `hsl(182, 95%, 85%)` / `#B7FBFD` | `--mint-cool-10` |
| ![Paletas](imagens/D9FEFF.png) | `Mint Cool 5`  |  `0.890`   | `hsl(182, 100%, 93%)` / `#D9FEFF` | `--mint-cool-5`  |

### Mint Cool Vivid

|              Cor               |     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :----------------------------: | :------------------: | :--------: | :-------------------------------: | :--------------------: |
| ![Paletas](imagens/0E3E42.png) | `Mint Cool Vivid 80` |  `0.025`   | `hsl(185, 65%, 16%)` / `#0E3E42`  | `--mint-cool-vivid-80` |
| ![Paletas](imagens/006257.png) | `Mint Cool Vivid 70` |  `0.054`   | `hsl(173, 100%, 19%)` / `#006257`  | `--mint-cool-vivid-70` |
| ![Paletas](imagens/00797E.png) | `Mint Cool Vivid 60` |  `0.100`   | `hsl(182, 100%, 25%)` / `#00797E`  | `--mint-cool-vivid-60` |
| ![Paletas](imagens/00868E.png) | `Mint Cool Vivid 50` |  `0.180`   | `hsl(183, 100%, 28%)` / `#00868E` | `--mint-cool-vivid-50` |
| ![Paletas](imagens/00AAAD.png) | `Mint Cool Vivid 40` |  `0.283`   | `hsl(181, 100%, 34%)` / `#00AAAD`  | `--mint-cool-vivid-40` |
| ![Paletas](imagens/00E7DC.png) | `Mint Cool Vivid 30` |  `0.419`   | `hsl(177, 100%, 45%)` / `#00E7DC`  | `--mint-cool-vivid-30` |
| ![Paletas](imagens/12FFF5.png) | `Mint Cool Vivid 20` |  `0.589`   | `hsl(177, 100%, 54%)` / `#12FFF5`  | `--mint-cool-vivid-20` |
| ![Paletas](imagens/7EFFEE.png) | `Mint Cool Vivid 10` |  `0.788`   | `hsl(172, 100%, 75%)` / `#7EFFEE`  | `--mint-cool-vivid-10` |
| ![Paletas](imagens/D2FFF9.png) | `Mint Cool Vivid 5`  |  `0.896`   | `hsl(172, 100%, 91%)` / `#D2FFF9`  | `--mint-cool-vivid-5`  |

### Cyan

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :----------------------------: | :---------: | :--------: | :------------------------------: | :---------: |
| ![Paletas](imagens/151E21.png) |  `Cyan 90`  |  `0.008`   | `hsl(195, 22%, 11%)` / `#151E21`  | `--cyan-90` |
| ![Paletas](imagens/1F3A40.png) |  `Cyan 80`  |  `0.027`   | `hsl(191, 35%, 19%)` / `#1F3A40` | `--cyan-80` |
| ![Paletas](imagens/25535F.png) |  `Cyan 70`  |  `0.059`   | `hsl(192, 44%, 26%)` / `#25535F` | `--cyan-70` |
| ![Paletas](imagens/187089.png) |  `Cyan 60`  |  `0.107`   | `hsl(193, 70%, 32%)` / `#187089` | `--cyan-60` |
| ![Paletas](imagens/0088B1.png) |  `Cyan 50`  |  `0.176`   | `hsl(194, 100%, 35%)` / `#0088B1` | `--cyan-50` |
| ![Paletas](imagens/23ACD3.png) |  `Cyan 40`  |  `0.283`   | `hsl(193, 72%, 48%)` / `#23ACD3` | `--cyan-40` |
| ![Paletas](imagens/37CFFC.png) |  `Cyan 30`  |  `0.446`   | `hsl(194, 97%, 60%)` / `#37CFFC` | `--cyan-30` |
| ![Paletas](imagens/88E3FF.png) |  `Cyan 20`  |  `0.649`   | `hsl(194, 100%, 77%)` / `#88E3FF` | `--cyan-20` |
| ![Paletas](imagens/C0F1FF.png) |  `Cyan 10`  |  `0.792`   | `hsl(193, 100%, 88%)` / `#C0F1FF` | `--cyan-10` |
| ![Paletas](imagens/E2F8FE.png) |  `Cyan 5`   |  `0.896`   | `hsl(193, 93%, 94%)` / `#E2F8FE` | `--cyan-5`  |

### Cyan Vivid

|              Cor               |   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :----------------------------: | :-------------: | :--------: | :-------------------------------: | :---------------: |
| ![Paletas](imagens/004359.png) | `Cyan Vivid 80` |  `0.036`   | `hsl(195, 100%, 17%)` / `#004359`  | `--cyan-vivid-80` |
| ![Paletas](imagens/005875.png) | `Cyan Vivid 70` |  `0.064`   | `hsl(195, 100%, 23%)` / `#005875`  | `--cyan-vivid-70` |
| ![Paletas](imagens/006588.png) | `Cyan Vivid 60` |  `0.113`   | `hsl(195, 100%, 27%)` / `#006588` | `--cyan-vivid-60` |
| ![Paletas](imagens/007AAB.png) | `Cyan Vivid 50` |  `0.182`   | `hsl(197, 100%, 34%)` / `#007AAB` | `--cyan-vivid-50` |
| ![Paletas](imagens/0094CA.png) | `Cyan Vivid 40` |  `0.283`   | `hsl(196, 100%, 40%)` / `#0094CA` | `--cyan-vivid-40` |
| ![Paletas](imagens/00AFEB.png) | `Cyan Vivid 30` |  `0.419`   | `hsl(195, 100%, 46%)` / `#00AFEB` | `--cyan-vivid-30` |
| ![Paletas](imagens/4AD5FF.png) | `Cyan Vivid 20` |  `0.583`   | `hsl(194, 100%, 65%)` / `#4AD5FF`  | `--cyan-vivid-20` |
| ![Paletas](imagens/AAEBFF.png) | `Cyan Vivid 10` |  `0.790`   | `hsl(194, 100%, 83%)` / `#AAEBFF` | `--cyan-vivid-10` |
| ![Paletas](imagens/E6F8FF.png) | `Cyan Vivid 5`  |  `0.922`   | `hsl(197, 100%, 95%)` / `#E6F8FF` | `--cyan-vivid-5`  |

### Blue

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :----------------------------: | :---------: | :--------: | :------------------------------: | :---------: |
| ![Paletas](imagens/0E1C2A.png) |  `Blue 90`  |  `0.008`   | `hsl(210, 50%, 11%)` / `#0E1C2A`  | `--blue-90` |
| ![Paletas](imagens/0B3059.png) |  `Blue 80`  |  `0.027`   | `hsl(212, 78%, 20%)` / `#0B3059` | `--blue-80` |
| ![Paletas](imagens/00418D.png) |  `Blue 70`  |  `0.059`   | `hsl(212, 100%, 28%)` / `#00418D` | `--blue-70` |
| ![Paletas](imagens/0054B7.png) |  `Blue 60`  |  `0.107`   | `hsl(212, 100%, 36%)` / `#0054B7` | `--blue-60` |
| ![Paletas](imagens/0067E5.png) |  `Blue 50`  |  `0.177`   | `hsl(213, 100%, 45%)` / `#0067E5` | `--blue-50` |
| ![Paletas](imagens/1B86FF.png) |  `Blue 40`  |  `0.283`   | `hsl(212, 100%, 55%)` / `#1B86FF` | `--blue-40` |
| ![Paletas](imagens/52A2FF.png) |  `Blue 30`  |  `0.416`   | `hsl(212, 100%, 66%)` / `#52A2FF` | `--blue-30` |
| ![Paletas](imagens/8ABCFF.png) |  `Blue 20`  |  `0.582`   | `hsl(214, 100%, 77%)` / `#8ABCFF` | `--blue-20` |
| ![Paletas](imagens/C6D8FF.png) |  `Blue 10`  |  `0.797`   | `hsl(221, 100%, 89%)` / `#C6D8FF` | `--blue-10` |
| ![Paletas](imagens/DCEAFA.png) |  `Blue 5`   |  `0.912`   | `hsl(212, 75%, 92%)` / `#DCEAFA` | `--blue-5`  |

### Blue Vivid

|              Cor               |   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :----------------------------: | :-------------: | :--------: | :-------------------------------: | :---------------: |
| ![Paletas](imagens/002967.png) | `Blue Vivid 80` |  `0.027`   | `hsl(216, 100%, 20%)` / `#002967`  | `--blue-vivid-80` |
| ![Paletas](imagens/003F88.png) | `Blue Vivid 70` |  `0.059`   | `hsl(212, 100%, 27%)` / `#003F88`  | `--blue-vivid-70` |
| ![Paletas](imagens/0052A5.png) | `Blue Vivid 60` |  `0.106`   | `hsl(210, 100%, 32%)` / `#0052A5` | `--blue-vivid-60` |
| ![Paletas](imagens/0064D5.png) | `Blue Vivid 50` |  `0.178`   | `hsl(212, 100%, 42%)` / `#0064D5` | `--blue-vivid-50` |
| ![Paletas](imagens/1E7BFF.png) | `Blue Vivid 40` |  `0.278`   | `hsl(215, 100%, 56%)` / `#1E7BFF` | `--blue-vivid-40` |
| ![Paletas](imagens/4FA0FF.png) | `Blue Vivid 30` |  `0.419`   | `hsl(212, 100%, 65%)` / `#4FA0FF` | `--blue-vivid-30` |
| ![Paletas](imagens/94C4FF.png) | `Blue Vivid 20` |  `0.613`   | `hsl(213, 100%, 79%)` / `#94C4FF` | `--blue-vivid-20` |
| ![Paletas](imagens/BEDBFF.png) | `Blue Vivid 10` |  `0.781`   | `hsl(213, 100%, 87%)` / `#BEDBFF` | `--blue-vivid-10` |
| ![Paletas](imagens/DAE8FB.png) | `Blue Vivid 5`  |  `0.896`   | `hsl(215, 80%, 92%)` / `#DAE8FB` | `--blue-vivid-5`  |

### Blue Cool

|              Cor               |  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :----------------------------: | :------------: | :--------: | :------------------------------: | :--------------: |
| ![Paletas](imagens/0A202B.png) | `Blue Cool 90` |  `0.008`   | `hsl(200, 62%, 10%)` / `#0A202B`  | `--blue-cool-90` |
| ![Paletas](imagens/003D59.png) | `Blue Cool 80` |  `0.028`   | `hsl(199, 100%, 17%)` / `#003D59` | `--blue-cool-80` |
| ![Paletas](imagens/005380.png) | `Blue Cool 70` |  `0.059`   | `hsl(201, 100%, 25%)` / `#005380` | `--blue-cool-70` |
| ![Paletas](imagens/0069A7.png) | `Blue Cool 60` |  `0.106`   | `hsl(202, 100%, 33%)` / `#0069A7` | `--blue-cool-60` |
| ![Paletas](imagens/0088CF.png) | `Blue Cool 50` |  `0.177`   | `hsl(201, 100%, 41%)` / `#0088CF` | `--blue-cool-50` |
| ![Paletas](imagens/289FE6.png) | `Blue Cool 40` |  `0.285`   | `hsl(202, 79%, 53%)` / `#289FE6` | `--blue-cool-40` |
| ![Paletas](imagens/47B7FA.png) | `Blue Cool 30` |  `0.416`   | `hsl(202, 95%, 63%)` / `#47B7FA` | `--blue-cool-30` |
| ![Paletas](imagens/82CEFA.png) | `Blue Cool 20` |  `0.586`   | `hsl(202, 92%, 75%)` / `#82CEFA` | `--blue-cool-20` |
| ![Paletas](imagens/C3E5F3.png) | `Blue Cool 10` |  `0.793`   | `hsl(197, 67%, 86%)` / `#C3E5F3` | `--blue-cool-10` |
| ![Paletas](imagens/D4EBF6.png) | `Blue Cool 5`  |  `0.870`   | `hsl(199, 65%, 90%)` / `#D4EBF6` | `--blue-cool-5`  |

### Blue Cool Vivid

|              Cor               |     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :----------------------------: | :------------------: | :--------: | :-------------------------------: | :--------------------: |
| ![Paletas](imagens/002E48.png) | `Blue Cool Vivid 80` |  `0.022`   | `hsl(202, 100%, 14%)` / `#002E48` | `--blue-cool-vivid-80` |
| ![Paletas](imagens/004876.png) | `Blue Cool Vivid 70` |  `0.060`   | `hsl(203, 100%, 23%)` / `#004876`  | `--blue-cool-vivid-70` |
| ![Paletas](imagens/005B98.png) | `Blue Cool Vivid 60` |  `0.110`   | `hsl(204, 100%, 30%)` / `#005B98`  | `--blue-cool-vivid-60` |
| ![Paletas](imagens/0076B2.png) | `Blue Cool Vivid 50` |  `0.176`   | `hsl(200, 100%, 35%)` / `#0076B2`  | `--blue-cool-vivid-50` |
| ![Paletas](imagens/009DF0.png) | `Blue Cool Vivid 40` |  `0.299`   | `hsl(201, 100%, 47%)` / `#009DF0`  | `--blue-cool-vivid-40` |
| ![Paletas](imagens/32B5FF.png) | `Blue Cool Vivid 30` |  `0.420`   | `hsl(202, 100%, 60%)` / `#32B5FF`  | `--blue-cool-vivid-30` |
| ![Paletas](imagens/77CFFF.png) | `Blue Cool Vivid 20` |  `0.596`   | `hsl(201, 100%, 73%)` / `#77CFFF`  | `--blue-cool-vivid-20` |
| ![Paletas](imagens/AEE3FF.png) | `Blue Cool Vivid 10` |  `0.779`   | `hsl(201, 100%, 84%)` / `#AEE3FF`  | `--blue-cool-vivid-10` |
| ![Paletas](imagens/C8EDFE.png) | `Blue Cool Vivid 5`  |  `0.868`   | `hsl(199, 96%, 89%)` / `#C8EDFE`  | `--blue-cool-vivid-5`  |

### Blue Warm

|              Cor               |  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :----------------------------: | :------------: | :--------: | :------------------------------: | :--------------: |
| ![Paletas](imagens/0D152C.png) | `Blue Warm 90` |  `0.008`   | `hsl(225, 54%, 11%)` / `#0D152C` | `--blue-warm-90` |
| ![Paletas](imagens/162A54.png) | `Blue Warm 80` |  `0.027`   | `hsl(221, 58%, 21%)` / `#162A54` | `--blue-warm-80` |
| ![Paletas](imagens/053294.png) | `Blue Warm 70` |  `0.059`   | `hsl(221, 93%, 30%)` / `#053294` | `--blue-warm-70` |
| ![Paletas](imagens/0041CA.png) | `Blue Warm 60` |  `0.107`   | `hsl(221, 100%, 40%)` / `#0041CA` | `--blue-warm-60` |
| ![Paletas](imagens/0056F9.png) | `Blue Warm 50` |  `0.179`   | `hsl(219, 100%, 49%)` / `#0056F9` | `--blue-warm-50` |
| ![Paletas](imagens/346EFF.png) | `Blue Warm 40` |  `0.282`   | `hsl(223, 100%, 60%)` / `#346EFF` | `--blue-warm-40` |
| ![Paletas](imagens/6794F9.png) | `Blue Warm 30` |  `0.419`   | `hsl(222, 92%, 69%)` / `#6794F9` | `--blue-warm-30` |
| ![Paletas](imagens/A3BEFF.png) | `Blue Warm 20` |  `0.649`   | `hsl(222, 100%, 82%)` / `#A3BEFF` | `--blue-warm-20` |
| ![Paletas](imagens/CCD8F3.png) | `Blue Warm 10` |  `0.795`   | `hsl(222, 62%, 88%)` / `#CCD8F3` | `--blue-warm-10` |
| ![Paletas](imagens/DBE3F4.png) | `Blue Warm 5`  |  `0.874`   | `hsl(221, 53%, 91%)` / `#DBE3F4` | `--blue-warm-5`  |

### Blue Warm Vivid

|              Cor               |     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :----------------------------: | :------------------: | :--------: | :-------------------------------: | :--------------------: |
| ![Paletas](imagens/001851.png) | `Blue Warm Vivid 90` |  `0.013`   | `hsl(222, 100%, 16%)` / `#001851`  | `--blue-warm-vivid-90` |
| ![Paletas](imagens/002681.png) | `Blue Warm Vivid 80` |  `0.035`   | `hsl(222, 100%, 25%)` / `#002681`  | `--blue-warm-vivid-80` |
| ![Paletas](imagens/003DC7.png) | `Blue Warm Vivid 70` |  `0.093`   | `hsl(222, 100%, 39%)` / `#003DC7`  | `--blue-warm-vivid-70` |
| ![Paletas](imagens/0043DE.png) | `Blue Warm Vivid 60` |  `0.119`   | `hsl(222, 100%, 44%)` / `#0043DE`  | `--blue-warm-vivid-60` |
| ![Paletas](imagens/0B52FF.png) | `Blue Warm Vivid 50` |  `0.178`   | `hsl(223, 100%, 52%)` / `#0B52FF`  | `--blue-warm-vivid-50` |
| ![Paletas](imagens/3F79FF.png) | `Blue Warm Vivid 40` |  `0.287`   | `hsl(222, 100%, 62%)` / `#3F79FF`  | `--blue-warm-vivid-40` |
| ![Paletas](imagens/729BFF.png) | `Blue Warm Vivid 30` |  `0.419`   | `hsl(223, 100%, 72%)` / `#729BFF`  | `--blue-warm-vivid-30` |
| ![Paletas](imagens/9EBBFF.png) | `Blue Warm Vivid 20` |  `0.597`   | `hsl(222, 100%, 81%)` / `#9EBBFF` | `--blue-warm-vivid-20` |
| ![Paletas](imagens/C3D5FF.png) | `Blue Warm Vivid 10` |  `0.772`   | `hsl(222, 100%, 88%)` / `#C3D5FF` | `--blue-warm-vivid-10` |
| ![Paletas](imagens/DFE7F9.png) | `Blue Warm Vivid 5`  |  `0.905`   | `hsl(222, 68%, 93%)` / `#DFE7F9` | `--blue-warm-vivid-5`  |

### Indigo

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal         |     Token     |
| :----------------------------: | :---------: | :--------: | :------------------------------: | :-----------: |
| ![Paletas](imagens/13132A.png) | `Indigo 90` |  `0.008`   | `hsl(240, 38%, 12%)` / `#13132A` | `--indigo-90` |
| ![Paletas](imagens/1F1D52.png) | `Indigo 80` |  `0.026`   | `hsl(242, 48%, 22%)` / `#1F1D52` | `--indigo-80` |
| ![Paletas](imagens/1611A3.png) | `Indigo 70` |  `0.059`   | `hsl(242, 81%, 35%)` / `#1611A3` | `--indigo-70` |
| ![Paletas](imagens/0800F8.png) | `Indigo 60` |  `0.107`   | `hsl(242, 100%, 49%)` / `#0800F8` | `--indigo-60` |
| ![Paletas](imagens/3029FF.png) | `Indigo 50` |  `0.177`   | `hsl(242, 100%, 58%)` / `#3029FF` | `--indigo-50` |
| ![Paletas](imagens/655AFF.png) | `Indigo 40` |  `0.282`   | `hsl(244, 100%, 68%)` / `#655AFF` | `--indigo-40` |
| ![Paletas](imagens/8984FF.png) | `Indigo 30` |  `0.420`   | `hsl(242, 100%, 76%)` / `#8984FF` | `--indigo-30` |
| ![Paletas](imagens/B1AAFF.png) | `Indigo 20` |  `0.582`   | `hsl(245, 100%, 83%)` / `#B1AAFF` | `--indigo-20` |
| ![Paletas](imagens/D4CEFD.png) | `Indigo 10` |  `0.790`   | `hsl(248, 92%, 90%)` / `#D4CEFD` | `--indigo-10` |
| ![Paletas](imagens/E0DEF5.png) | `Indigo 5`  |  `0.868`   | `hsl(245, 53%, 92%)` / `#E0DEF5` | `--indigo-5`  |

### Indigo Vivid

|              Cor               |    Nome da Cor    | Luminância |         HSL / Hexadecimal         |        Token        |
| :----------------------------: | :---------------: | :--------: | :-------------------------------: | :-----------------: |
| ![Paletas](imagens/050088.png) | `Indigo Vivid 80` |  `0.024`   | `hsl(242, 100%, 27%)` / `#050088`  | `--indigo-vivid-80` |
| ![Paletas](imagens/1200D5.png) | `Indigo Vivid 70` |  `0.057`   | `hsl(245, 100%, 42%)` / `#1200D5`  | `--indigo-vivid-70` |
| ![Paletas](imagens/110BFF.png) | `Indigo Vivid 60` |  `0.111`   | `hsl(241, 100%, 52%)` / `#110BFF`  | `--indigo-vivid-60` |
| ![Paletas](imagens/3B35FF.png) | `Indigo Vivid 50` |  `0.181`   | `hsl(242, 100%, 60%)` / `#3B35FF`  | `--indigo-vivid-50` |
| ![Paletas](imagens/7A76FF.png) | `Indigo Vivid 40` |  `0.298`   | `hsl(242, 100%, 73%)` / `#7A76FF` | `--indigo-vivid-40` |
| ![Paletas](imagens/9691FF.png) | `Indigo Vivid 30` |  `0.423`   | `hsl(243, 100%, 78%)` / `#9691FF`  | `--indigo-vivid-30` |
| ![Paletas](imagens/BEBBFF.png) | `Indigo Vivid 20` |  `0.641`   | `hsl(243, 100%, 87%)` / `#BEBBFF` | `--indigo-vivid-20` |
| ![Paletas](imagens/D3D0FD.png) | `Indigo Vivid 10` |  `0.763`   | `hsl(244, 92%, 90%)` / `#D3D0FD` | `--indigo-vivid-10` |
| ![Paletas](imagens/E5E3F9.png) | `Indigo Vivid 5`  |  `0.880`   | `hsl(245, 65%, 93%)` / `#E5E3F9` | `--indigo-vivid-5`  |

### Indigo Cool

|              Cor               |   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :----------------------------: | :--------------: | :--------: | :------------------------------: | :----------------: |
| ![Paletas](imagens/0F0F31.png) | `Indigo Cool 90` |  `0.008`   | `hsl(240, 53%, 13%)` / `#0F0F31` | `--indigo-cool-90` |
| ![Paletas](imagens/1A1F58.png) | `Indigo Cool 80` |  `0.027`   | `hsl(235, 54%, 22%)` / `#1A1F58` | `--indigo-cool-80` |
| ![Paletas](imagens/0818A3.png) | `Indigo Cool 70` |  `0.059`   | `hsl(234, 91%, 34%)` / `#0818A3` | `--indigo-cool-70` |
| ![Paletas](imagens/0022E2.png) | `Indigo Cool 60` |  `0.106`   | `hsl(231, 100%, 44%)` / `#0022E2` | `--indigo-cool-60` |
| ![Paletas](imagens/1C46FF.png) | `Indigo Cool 50` |  `0.177`   | `hsl(229, 100%, 55%)` / `#1C46FF` | `--indigo-cool-50` |
| ![Paletas](imagens/4C6EFF.png) | `Indigo Cool 40` |  `0.282`   | `hsl(229, 100%, 65%)` / `#4C6EFF` | `--indigo-cool-40` |
| ![Paletas](imagens/798DFF.png) | `Indigo Cool 30` |  `0.417`   | `hsl(231, 100%, 74%)` / `#798DFF` | `--indigo-cool-30` |
| ![Paletas](imagens/A2B0FF.png) | `Indigo Cool 20` |  `0.584`   | `hsl(231, 100%, 82%)` / `#A2B0FF` | `--indigo-cool-20` |
| ![Paletas](imagens/C9D1FE.png) | `Indigo Cool 10` |  `0.794`   | `hsl(231, 96%, 89%)` / `#C9D1FE` | `--indigo-cool-10` |
| ![Paletas](imagens/DCDFF8.png) | `Indigo Cool 5`  |  `0.873`   | `hsl(234, 67%, 92%)` / `#DCDFF8` | `--indigo-cool-5`  |

### Indigo Cool Vivid

|              Cor               |      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :----------------------------: | :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| ![Paletas](imagens/000CA2.png) | `Indigo Cool Vivid 80` |  `0.036`   | `hsl(236, 100%, 32%)` / `#000CA2`  | `--indigo-cool-vivid-80` |
| ![Paletas](imagens/0001E0.png) | `Indigo Cool Vivid 70` |  `0.061`   | `hsl(240, 100%, 44%)` / `#0001E0`  | `--indigo-cool-vivid-70` |
| ![Paletas](imagens/2528FF.png) | `Indigo Cool Vivid 60` |  `0.124`   | `hsl(239, 100%, 57%)` / `#2528FF`  | `--indigo-cool-vivid-60` |
| ![Paletas](imagens/4050FF.png) | `Indigo Cool Vivid 50` |  `0.181`   | `hsl(235, 100%, 63%)` / `#4050FF` | `--indigo-cool-vivid-50` |
| ![Paletas](imagens/4D74FF.png) | `Indigo Cool Vivid 40` |  `0.284`   | `hsl(227, 100%, 65%)` / `#4D74FF`  | `--indigo-cool-vivid-40` |
| ![Paletas](imagens/8798FF.png) | `Indigo Cool Vivid 30` |  `0.434`   | `hsl(232, 100%, 76%)` / `#8798FF` | `--indigo-cool-vivid-30` |
| ![Paletas](imagens/A9B5FF.png) | `Indigo Cool Vivid 20` |  `0.587`   | `hsl(232, 100%, 83%)` / `#A9B5FF` | `--indigo-cool-vivid-20` |
| ![Paletas](imagens/CDD3FD.png) | `Indigo Cool Vivid 10` |  `0.787`   | `hsl(233, 92%, 90%)` / `#CDD3FD` | `--indigo-cool-vivid-10` |
| ![Paletas](imagens/DFE2F9.png) | `Indigo Cool Vivid 5`  |  `0.875`   | `hsl(233, 68%, 93%)` / `#DFE2F9` | `--indigo-cool-vivid-5`  |

### Indigo Warm

|              Cor               |   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :----------------------------: | :--------------: | :--------: | :------------------------------: | :----------------: |
| ![Paletas](imagens/1A1427.png) | `Indigo Warm 90` |  `0.008`   | `hsl(259, 32%, 12%)` / `#1A1427` | `--indigo-warm-90` |
| ![Paletas](imagens/292052.png) | `Indigo Warm 80` |  `0.027`   | `hsl(251, 44%, 22%)` / `#292052` | `--indigo-warm-80` |
| ![Paletas](imagens/2E0BAD.png) | `Indigo Warm 70` |  `0.059`   | `hsl(253, 88%, 36%)` / `#2E0BAD` | `--indigo-warm-70` |
| ![Paletas](imagens/4415D9.png) | `Indigo Warm 60` |  `0.107`   | `hsl(254, 82%, 47%)` / `#4415D9` | `--indigo-warm-60` |
| ![Paletas](imagens/612FFF.png) | `Indigo Warm 50` |  `0.177`   | `hsl(254, 100%, 59%)` / `#612FFF` | `--indigo-warm-50` |
| ![Paletas](imagens/7C55FF.png) | `Indigo Warm 40` |  `0.283`   | `hsl(254, 100%, 67%)` / `#7C55FF` | `--indigo-warm-40` |
| ![Paletas](imagens/9F81FF.png) | `Indigo Warm 30` |  `0.418`   | `hsl(254, 100%, 75%)` / `#9F81FF` | `--indigo-warm-30` |
| ![Paletas](imagens/BAA8FF.png) | `Indigo Warm 20` |  `0.585`   | `hsl(252, 100%, 83%)` / `#BAA8FF` | `--indigo-warm-20` |
| ![Paletas](imagens/D9CDFD.png) | `Indigo Warm 10` |  `0.788`   | `hsl(255, 92%, 90%)` / `#D9CDFD` | `--indigo-warm-10` |
| ![Paletas](imagens/E7DFF3.png) | `Indigo Warm 5`  |  `0.871`   | `hsl(264, 45%, 91%)` / `#E7DFF3` | `--indigo-warm-5`  |

### Indigo Warm Vivid

|              Cor               |      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :----------------------------: | :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| ![Paletas](imagens/1B007F.png) | `Indigo Warm Vivid 80` |  `0.021`   | `hsl(253, 100%, 25%)` / `#1B007F`  | `--indigo-warm-vivid-80` |
| ![Paletas](imagens/2D00CA.png) | `Indigo Warm Vivid 70` |  `0.052`   | `hsl(253, 100%, 40%)` / `#2D00CA`  | `--indigo-warm-vivid-70` |
| ![Paletas](imagens/4B10FF.png) | `Indigo Warm Vivid 60` |  `0.106`   | `hsl(255, 100%, 53%)` / `#4B10FF`  | `--indigo-warm-vivid-60` |
| ![Paletas](imagens/6E41FF.png) | `Indigo Warm Vivid 50` |  `0.177`   | `hsl(254, 100%, 63%)` / `#6E41FF`  | `--indigo-warm-vivid-50` |
| ![Paletas](imagens/966FFF.png) | `Indigo Warm Vivid 40` |  `0.283`   | `hsl(256, 100%, 72%)` / `#966FFF`  | `--indigo-warm-vivid-40` |
| ![Paletas](imagens/B492FF.png) | `Indigo Warm Vivid 30` |  `0.419`   | `hsl(259, 100%, 79%)` / `#B492FF` | `--indigo-warm-vivid-30` |
| ![Paletas](imagens/C6B2FF.png) | `Indigo Warm Vivid 20` |  `0.598`   | `hsl(256, 100%, 85%)` / `#C6B2FF`  | `--indigo-warm-vivid-20` |
| ![Paletas](imagens/DBCDFD.png) | `Indigo Warm Vivid 10` |  `0.759`   | `hsl(257, 92%, 90%)` / `#DBCDFD` | `--indigo-warm-vivid-10` |
| ![Paletas](imagens/EAE5F8.png) | `Indigo Warm Vivid 5`  |  `0.901`   | `hsl(256, 58%, 94%)` / `#EAE5F8` | `--indigo-warm-vivid-5`  |

### Violet

|              Cor               | Nome da Cor | Luminância |        HSL / Hexadecimal         |     Token     |
| :----------------------------: | :---------: | :--------: | :------------------------------: | :-----------: |
| ![Paletas](imagens/1A1427.png) | `Violet 90` |  `0.008`   | `hsl(259, 32%, 12%)` / `#1A1427` | `--violet-90` |
| ![Paletas](imagens/331F51.png) | `Violet 80` |  `0.027`   | `hsl(264, 45%, 22%)` / `#331F51` | `--violet-80` |
| ![Paletas](imagens/4C1C8C.png) | `Violet 70` |  `0.058`   | `hsl(266, 67%, 33%)` / `#4C1C8C` | `--violet-70` |
| ![Paletas](imagens/6120BF.png) | `Violet 60` |  `0.107`   | `hsl(265, 71%, 44%)` / `#6120BF` | `--violet-60` |
| ![Paletas](imagens/7A2CEA.png) | `Violet 50` |  `0.178`   | `hsl(265, 82%, 55%)` / `#7A2CEA` | `--violet-50` |
| ![Paletas](imagens/974DFF.png) | `Violet 40` |  `0.283`   | `hsl(265, 100%, 65%)` / `#974DFF` | `--violet-40` |
| ![Paletas](imagens/B279FF.png) | `Violet 30` |  `0.415`   | `hsl(266, 100%, 74%)` / `#B279FF` | `--violet-30` |
| ![Paletas](imagens/C69FFF.png) | `Violet 20` |  `0.583`   | `hsl(264, 100%, 81%)` / `#C69FFF` | `--violet-20` |
| ![Paletas](imagens/E3CCFE.png) | `Violet 10` |  `0.794`   | `hsl(268, 96%, 90%)` / `#E3CCFE` | `--violet-10` |
| ![Paletas](imagens/E9E1F5.png) | `Violet 5`  |  `0.889`   | `hsl(264, 50%, 92%)` / `#E9E1F5` | `--violet-5`  |

### Violet Vivid

|              Cor               |    Nome da Cor    | Luminância |         HSL / Hexadecimal         |        Token        |
| :----------------------------: | :---------------: | :--------: | :-------------------------------: | :-----------------: |
| ![Paletas](imagens/411178.png) | `Violet Vivid 80` |  `0.027`   | `hsl(268, 75%, 27%)` / `#411178`  | `--violet-vivid-80` |
| ![Paletas](imagens/6207B7.png) | `Violet Vivid 70` |  `0.053`   | `hsl(271, 93%, 37%)` / `#6207B7`  | `--violet-vivid-70` |
| ![Paletas](imagens/8B13E8.png) | `Violet Vivid 60` |  `0.107`   | `hsl(274, 85%, 49%)` / `#8B13E8`  | `--violet-vivid-60` |
| ![Paletas](imagens/A338FF.png) | `Violet Vivid 50` |  `0.178`   | `hsl(272, 100%, 61%)` / `#A338FF`  | `--violet-vivid-50` |
| ![Paletas](imagens/B968FF.png) | `Violet Vivid 40` |  `0.284`   | `hsl(272, 100%, 70%)` / `#B968FF`  | `--violet-vivid-40` |
| ![Paletas](imagens/CE8DFF.png) | `Violet Vivid 30` |  `0.417`   | `hsl(274, 100%, 78%)` / `#CE8DFF`  | `--violet-vivid-30` |
| ![Paletas](imagens/DBC1FF.png) | `Violet Vivid 20` |  `0.586`   | `hsl(265, 100%, 88%)` / `#DBC1FF` | `--violet-vivid-20` |
| ![Paletas](imagens/EFE4FF.png) | `Violet Vivid 10` |  `0.801`   | `hsl(264, 100%, 95%)` / `#EFE4FF` | `--violet-vivid-10` |
| ![Paletas](imagens/F8F2FF.png) | `Violet Vivid 5`  |  `0.904`   | `hsl(268, 100%, 97%)` / `#F8F2FF` | `--violet-vivid-5`  |

### Violet Warm

|              Cor               |   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :----------------------------: | :--------------: | :--------: | :------------------------------: | :----------------: |
| ![Paletas](imagens/231922.png) | `Violet Warm 90` |  `0.008`   | `hsl(306, 17%, 12%)` / `#231922`  | `--violet-warm-90` |
| ![Paletas](imagens/422A3E.png) | `Violet Warm 80` |  `0.026`   | `hsl(310, 22%, 21%)` / `#422A3E` | `--violet-warm-80` |
| ![Paletas](imagens/6C3165.png) | `Violet Warm 70` |  `0.059`   | `hsl(307, 38%, 31%)` / `#6C3165` | `--violet-warm-70` |
| ![Paletas](imagens/A1308E.png) | `Violet Warm 60` |  `0.106`   | `hsl(310, 54%, 41%)` / `#A1308E` | `--violet-warm-60` |
| ![Paletas](imagens/E125E9.png) | `Violet Warm 50` |  `0.178`   | `hsl(298, 82%, 53%)` / `#E125E9` | `--violet-warm-50` |
| ![Paletas](imagens/E25DE7.png) | `Violet Warm 40` |  `0.284`   | `hsl(298, 74%, 64%)` / `#E25DE7` | `--violet-warm-40` |
| ![Paletas](imagens/ED86EF.png) | `Violet Warm 30` |  `0.417`   | `hsl(299, 77%, 73%)` / `#ED86EF` | `--violet-warm-30` |
| ![Paletas](imagens/F3B2F1.png) | `Violet Warm 20` |  `0.585`   | `hsl(302, 73%, 83%)` / `#F3B2F1` | `--violet-warm-20` |
| ![Paletas](imagens/FFD9FF.png) | `Violet Warm 10` |  `0.791`   | `hsl(300, 100%, 93%)` / `#FFD9FF` | `--violet-warm-10` |
| ![Paletas](imagens/FBECFC.png) | `Violet Warm 5`  |  `0.891`   | `hsl(296, 73%, 96%)` / `#FBECFC` | `--violet-warm-5`  |

### Violet Warm Vivid

|              Cor               |      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :----------------------------: | :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| ![Paletas](imagens/60074C.png) | `Violet Warm Vivid 80` |  `0.022`   | `hsl(313, 86%, 20%)` / `#60074C`  | `--violet-warm-vivid-80` |
| ![Paletas](imagens/93067E.png) | `Violet Warm Vivid 70` |  `0.055`   | `hsl(309, 92%, 30%)` / `#93067E`  | `--violet-warm-vivid-70` |
| ![Paletas](imagens/B8179E.png) | `Violet Warm Vivid 60` |  `0.105`   | `hsl(310, 78%, 41%)` / `#B8179E`  | `--violet-warm-vivid-60` |
| ![Paletas](imagens/F70AFF.png) | `Violet Warm Vivid 50` |  `0.177`   | `hsl(298, 100%, 52%)` / `#F70AFF`  | `--violet-warm-vivid-50` |
| ![Paletas](imagens/F250FF.png) | `Violet Warm Vivid 40` |  `0.283`   | `hsl(296, 100%, 66%)` / `#F250FF`  | `--violet-warm-vivid-40` |
| ![Paletas](imagens/F786FF.png) | `Violet Warm Vivid 30` |  `0.416`   | `hsl(296, 100%, 76%)` / `#F786FF` | `--violet-warm-vivid-30` |
| ![Paletas](imagens/FAB4FF.png) | `Violet Warm Vivid 20` |  `0.582`   | `hsl(296, 100%, 85%)` / `#FAB4FF` | `--violet-warm-vivid-20` |
| ![Paletas](imagens/FEDDFF.png) | `Violet Warm Vivid 10` |  `0.789`   | `hsl(298, 100%, 93%)` / `#FEDDFF` | `--violet-warm-vivid-10` |
| ![Paletas](imagens/FFF2FF.png) | `Violet Warm Vivid 5`  |  `0.917`   | `hsl(300, 100%, 97%)` / `#FFF2FF` | `--violet-warm-vivid-5`  |

### Magenta

|              Cor               | Nome da Cor  | Luminância |        HSL / Hexadecimal         |     Token      |
| :----------------------------: | :----------: | :--------: | :------------------------------: | :------------: |
| ![Paletas](imagens/231A1B.png) | `Magenta 90` |  `0.008`   | `hsl(353, 15%, 12%)` / `#231A1B` | `--magenta-90` |
| ![Paletas](imagens/4E2432.png) | `Magenta 80` |  `0.027`   | `hsl(340, 37%, 22%)` / `#4E2432` | `--magenta-80` |
| ![Paletas](imagens/7B2B47.png) | `Magenta 70` |  `0.059`   | `hsl(339, 48%, 33%)` / `#7B2B47` | `--magenta-70` |
| ![Paletas](imagens/A7305E.png) | `Magenta 60` |  `0.107`   | `hsl(337, 55%, 42%)` / `#A7305E` | `--magenta-60` |
| ![Paletas](imagens/FB166E.png) | `Magenta 50` |  `0.177`   | `hsl(337, 97%, 54%)` / `#FB166E` | `--magenta-50` |
| ![Paletas](imagens/FF4F91.png) | `Magenta 40` |  `0.284`   | `hsl(338, 100%, 65%)` / `#FF4F91` | `--magenta-40` |
| ![Paletas](imagens/FF82A4.png) | `Magenta 30` |  `0.419`   | `hsl(344, 100%, 75%)` / `#FF82A4` | `--magenta-30` |
| ![Paletas](imagens/FFAEC1.png) | `Magenta 20` |  `0.584`   | `hsl(346, 100%, 84%)` / `#FFAEC1` | `--magenta-20` |
| ![Paletas](imagens/FDDAE3.png) | `Magenta 10` |  `0.792`   | `hsl(345, 90%, 92%)` / `#FDDAE3` | `--magenta-10` |
| ![Paletas](imagens/FCECEE.png) | `Magenta 5`  |  `0.888`   | `hsl(353, 73%, 96%)` / `#FCECEE` | `--magenta-5`  |

### Magenta Vivid

|              Cor               |    Nome da Cor     | Luminância |         HSL / Hexadecimal         |        Token         |
| :----------------------------: | :----------------: | :--------: | :-------------------------------: | :------------------: |
| ![Paletas](imagens/680828.png) | `Magenta Vivid 80` |  `0.024`   | `hsl(340, 86%, 22%)` / `#680828`  | `--magenta-vivid-80` |
| ![Paletas](imagens/950639.png) | `Magenta Vivid 70` |  `0.050`   | `hsl(339, 92%, 30%)` / `#950639`  | `--magenta-vivid-70` |
| ![Paletas](imagens/D40057.png) | `Magenta Vivid 60` |  `0.106`   | `hsl(335, 100%, 42%)` / `#D40057`  | `--magenta-vivid-60` |
| ![Paletas](imagens/FF0C64.png) | `Magenta Vivid 50` |  `0.177`   | `hsl(338, 100%, 52%)` / `#FF0C64`  | `--magenta-vivid-50` |
| ![Paletas](imagens/FF4789.png) | `Magenta Vivid 40` |  `0.272`   | `hsl(338, 100%, 64%)` / `#FF4789`  | `--magenta-vivid-40` |
| ![Paletas](imagens/FF8AAA.png) | `Magenta Vivid 30` |  `0.418`   | `hsl(344, 100%, 77%)` / `#FF8AAA` | `--magenta-vivid-30` |
| ![Paletas](imagens/FFB6CA.png) | `Magenta Vivid 20` |  `0.584`   | `hsl(344, 100%, 86%)` / `#FFB6CA` | `--magenta-vivid-20` |
| ![Paletas](imagens/FFDEE8.png) | `Magenta Vivid 10` |  `0.789`   | `hsl(342, 100%, 94%)` / `#FFDEE8` | `--magenta-vivid-10` |
| ![Paletas](imagens/FFF2F4.png) | `Magenta Vivid 5`  |  `0.913`   | `hsl(351, 100%, 97%)` / `#FFF2F4` | `--magenta-vivid-5`  |

### Gray

A família `Gray` utiliza cores sem saturação entro de um círculo cromático. Representa uma família neutra.

|              Cor               | Nome da Cor | Luminância |         HSL / Hexadecimal       |    Token    |
| :----------------------------: | :---------: | :--------: | :-----------------------------: |:-----------:|
| ![Paletas](imagens/1B1B1B.png) |  `Gray 90`  |  `0.010`   |  `hsl(0, 0%, 11%)` / `#1B1B1B`  | `--gray-90` |
| ![Paletas](imagens/333333.png) |  `Gray 80`  |  `0.033`   |  `hsl(0, 0%, 20%)` / `#333333`  | `--gray-80` |
| ![Paletas](imagens/555555.png) |  `Gray 70`  |  `0.090`   |  `hsl(0, 0%, 33%)` / `#555555`  | `--gray-70` |
| ![Paletas](imagens/636363.png) |  `Gray 60`  |  `0.124`   |  `hsl(0, 0%, 39%)` / `#636363`  | `--gray-60` |
| ![Paletas](imagens/757575.png) |  `Gray 50`  |  `0.177`   |  `hsl(0, 0%, 46%)` / `#757575`  | `--gray-50` |
| ![Paletas](imagens/888888.png) |  `Gray 40`  |  `0.246`   |  `hsl(0, 0%, 53%)` / `#888888`  | `--gray-40` |
| ![Paletas](imagens/ADADAD.png) |  `Gray 30`  |  `0.417`   |  `hsl(0, 0%, 68%)` / `#ADADAD`  | `--gray-30` |
| ![Paletas](imagens/CCCCCC.png) |  `Gray 20`  |  `0.603`   |  `hsl(0, 0%, 80%)` / `#CCCCCC`  | `--gray-20` |
| ![Paletas](imagens/E6E6E6.png) |  `Gray 10`  |  `0.791`   |  `hsl(0, 0%, 90%)` / `#E6E6E6`  | `--gray-10` |
| ![Paletas](imagens/F0F0F0.png) |  `Gray 5`   |  `0.871`   |  `hsl(0, 0%, 94%)` / `#F0F0F0`  | `--gray-5`  |
| ![Paletas](imagens/F3F3F3.png) |  `Gray 4`   |  `0.896`   |  `hsl(0, 0%, 95%)` / `#F3F3F3`  | `--gray-4`  |
| ![Paletas](imagens/F6F6F6.png) |  `Gray 3`   |  `0.921`   |  `hsl(0, 0%, 96%)` / `#F6F6F6`  | `--gray-3`  |
| ![Paletas](imagens/F8F8F8.png) |  `Gray 2`   |  `0.938`   |  `hsl(0, 0%, 97%)` / `#F8F8F8`  | `--gray-2`  |
| ![Paletas](imagens/FCFCFC.png) |  `Gray 1`   |  `0.973`   |  `hsl(0, 0%, 99%)` / `#FCFCFC`  | `--gray-1`  |

### Gray Cool

A família `Gray Cool` utiliza cores com mínima saturação dentro de um círculo cromático. Representa uma família neutra com "tons frios" (azulado).

|              Cor               |   Nome da Cor  | Luminância |          HSL / Hexadecimal        |       Token       |
| :----------------------------: | :------------: | :--------: | :-------------------------------: |:-----------------:|
| ![Paletas](imagens/1C1D1F.png) | `Gray Cool 90` |  `0.012`   |  `hsl(220, 5%, 12%)` / `#1C1D1F`  | `--gray-cool-90` |
| ![Paletas](imagens/2D2E2F.png) | `Gray Cool 80` |  `0.027`   |  `hsl(210, 2%, 18%)` / `#2D2E2F`  | `--gray-cool-80` |
| ![Paletas](imagens/3D4551.png) | `Gray Cool 70` |  `0.058`   |  `hsl(216, 14%, 28%)` / `#3D4551` | `--gray-cool-70` |
| ![Paletas](imagens/565C65.png) | `Gray Cool 60` |  `0.105`   |  `hsl(216, 8%, 37%)` / `#565C65`  | `--gray-cool-60` |
| ![Paletas](imagens/71767A.png) | `Gray Cool 50` |  `0.178`   |  `hsl(207, 4%, 46%)` / `#71767A`  | `--gray-cool-50` |
| ![Paletas](imagens/8D9297.png) | `Gray Cool 40` |  `0.284`   |  `hsl(210, 5%, 57%)` / `#8D9297`  | `--gray-cool-40` |
| ![Paletas](imagens/A9AEB1.png) | `Gray Cool 30` |  `0.418`   |  `hsl(202, 5%, 68%)` / `#A9AEB1`  | `--gray-cool-30` |
| ![Paletas](imagens/C6CACE.png) | `Gray Cool 20` |  `0.587`   |  `hsl(210, 8%, 79%)` / `#C6CACE`  | `--gray-cool-20` |
| ![Paletas](imagens/DFE1E2.png) | `Gray Cool 10` |  `0.750`   |  `hsl(200, 5%, 88%)` / `#DFE1E2`  | `--gray-cool-10` |
| ![Paletas](imagens/EDEFF0.png) | `Gray Cool 5`  |  `0.860`   |  `hsl(200, 9%, 94%)` / `#EDEFF0`  | `--gray-cool-5`  |
| ![Paletas](imagens/F1F3F6.png) | `Gray Cool 4`  |  `0.894`   |  `hsl(216, 22%, 95%)` / `#F1F3F6` | `--gray-cool-4`  |
| ![Paletas](imagens/F5F6F7.png) | `Gray Cool 3`  |  `0.920`   |  `hsl(210, 11%, 96%)` / `#F5F6F7` | `--gray-cool-3`  |
| ![Paletas](imagens/F7F9FA.png) | `Gray Cool 2`  |  `0.944`   |  `hsl(200, 23%, 97%)` / `#F7F9FA` | `--gray-cool-2`  |
| ![Paletas](imagens/FBFCFD.png) | `Gray Cool 1`  |  `0.972`   |  `hsl(210, 33%, 99%)` / `#FBFCFD` | `--gray-cool-1`  |

### Gray Warm

A família `Gray Warm` utiliza cores com mínima saturação dentro de um círculo cromático. Representa uma família neutra com "tons quentes" (amarelados).

|              Cor               |   Nome da Cor  | Luminância |         HSL / Hexadecimal        |       Token      |
| :----------------------------: | :------------: | :--------: | :------------------------------: |:----------------:|
| ![Paletas](imagens/171716.png) | `Gray Warm 90` |  `0.008`   |  `hsl(60, 2%, 9%)` / `#171716`   | `--gray-warm-90` |
| ![Paletas](imagens/2E2E2A.png) | `Gray Warm 80` |  `0.027`   |  `hsl(60, 5%, 17%)` / `#2E2E2A`  | `--gray-warm-80` |
| ![Paletas](imagens/454540.png) | `Gray Warm 70` |  `0.058`   |  `hsl(60, 4%, 26%)` / `#454540`  | `--gray-warm-70` |
| ![Paletas](imagens/5D5D52.png) | `Gray Warm 60` |  `0.107`   |  `hsl(60, 6%, 34%)` / `#5D5D52`  | `--gray-warm-60` |
| ![Paletas](imagens/76766A.png) | `Gray Warm 50` |  `0.178`   |  `hsl(60, 5%, 44%)` / `#76766A`  | `--gray-warm-50` |
| ![Paletas](imagens/929285.png) | `Gray Warm 40` |  `0.283`   |  `hsl(60, 6%, 55%)` / `#929285`  | `--gray-warm-40` |
| ![Paletas](imagens/AFAEA2.png) | `Gray Warm 30` |  `0.419`   |  `hsl(55, 8%, 66%)` / `#AFAEA2`  | `--gray-warm-30` |
| ![Paletas](imagens/CAC9C0.png) | `Gray Warm 20` |  `0.581`   |  `hsl(54, 9%, 77%)` / `#CAC9C0`  | `--gray-warm-20` |
| ![Paletas](imagens/E6E6E2.png) | `Gray Warm 10` |  `0.789`   |  `hsl(60, 7%, 89%)` / `#E6E6E2`  | `--gray-warm-10` |
| ![Paletas](imagens/F0F0EC.png) | `Gray Warm 5`  |  `0.869`   |  `hsl(60, 12%, 93%)` / `#F0F0EC` | `--gray-warm-5`  |
| ![Paletas](imagens/F5F5F0.png) | `Gray Warm 4`  |  `0.910`   |  `hsl(60, 20%, 95%)` / `#F5F5F0` | `--gray-warm-4`  |
| ![Paletas](imagens/F6F6F2.png) | `Gray Warm 3`  |  `0.919`   |  `hsl(60, 18%, 96%)` / `#F6F6F2` | `--gray-warm-3`  |
| ![Paletas](imagens/F9F9F7.png) | `Gray Warm 2`  |  `0.946`   |  `hsl(60, 14%, 97%)` / `#F9F9F7` | `--gray-warm-2`  |
| ![Paletas](imagens/FCFCFB.png) | `Gray Warm 1`  |  `0.972`   |  `hsl(60, 14%, 99%)` / `#FCFCFB` | `--gray-warm-1`  |

### Pure

Esta família possui uma característica especial, que representa os extremos puros da luminância (branco e preto). Na prática, esta família pode estar presente em qualquer outra família da Paleta do Design System quando ampliada ou reduzida a luminância ao seu extremo, independente do matiz ou saturação. Por tanto, sua identificação como uma família de cores independente tem apenas objetivo didático.

|              Cor               | Nome da Cor | Luminância |          HSL / Hexadecimal       |    Token     |
| :----------------------------: | :---------: | :--------: | :------------------------------: |:------------:|
| ![Paletas](imagens/000000.png) | `Pure 100`  |  `0.000`   |  `hsl(0, 0%, 0%)` / `#000000`    | `--pure-100` |
| ![Paletas](imagens/FFFFFF.png) |  `Pure 0`   |  `1.000`   |  `hsl(0, 0%, 100%)` / `#FFFFFF`  |  `--pure-0`  |

---

## Tabela de Contraste

A seguir criamos uma tabela de contrastes com as principais cores das paletas apresentadas acima.

No *eixo horizontal* temos as principais cores utilizadas para textos e ícones e no *eixo vertical*, as principais cores utilizadas para superfícies.

**Legendas:**

- *AAA:* passa no nível AAA para texto de qualquer tamanho.
- *AA:* passa no nível AA para texto de qualquer tamanho e AAA para texto grande (acima de 18pt ou *bold* acima de 14pt).
- *x:* Contraste abaixo de 4,5. Não é recomendado no sistema de cores do Design System.
- *(número):* taxa de contraste segundo a WCAG 2.1.

| Superfície/Texto                       | ![#FFFFFF](imagens/FFFFFF.png) #FFFFFF | ![#333333](imagens/333333.png) #333333 | ![#003DC7](imagens/003DC7.png) #003DC7 | ![#A3BEFF](imagens/A3BEFF.png) #A3BEFF |
| -------------------------------------- | :------------------------------------: | :------------------------------------: | :------------------------------------: | :------------------------------------: |
| ![#FFFFFF](imagens/FFFFFF.png) #FFFFFF |                  ---                   |              AAA (12,63)               |               AAA (8,42)               |                   x                    |
| ![#F8F8F8](imagens/F8F8F8.png) #F8F8F8 |                   x                    |              AAA (11,89)               |               AA (7,93)                |                   x                    |
| ![#001851](imagens/001851.png) #001851 |              AAA (16,8)               |                   x                    |                   x                    |              AAA (9,09)               |
| ![#0043DE](imagens/0043DE.png) #0043DE |               AA (7,34)                |                   x                    |                   x                    |                   x                    |
| ![#C3D5FF](imagens/C3D5FF.png) #C3D5FF |                   x                    |               AAA (8,59)               |               AA (5,73)                |                   x                    |
| ![#009F89](imagens/009F89.png) #009F89 |               x                |                   x                    |                   x                    |                   x                    |
| ![#CAF7E7](imagens/CAF7E7.png) #CAF7E7 |                   x                    |              AAA (10,81)               |               AA (7,21)                |                   x                    |
| ![#FFE10E](imagens/FFE10E.png) #FFE10E |                   x                    |               AAA (9,64)               |               AA (6,43)                |                   x                    |
| ![#FFFAC4](imagens/FFFAC4.png) #FFFAC4 |                   x                    |              AAA (11,86)               |               AA (7,91)                |                   x                    |
| ![#EE1400](imagens/EE1400.png) #EE1400 |               AA (4,43)                |                   x                    |                   x                    |                   x                    |
| ![#FDB8AE](imagens/FFBFAE.png) #FFBFAE |                   x                    |               AAA (8,01)               |                   AA (5,34)                    |                   x                    |

Para checar qualquer combinação de cores não apresentadas na tabela, consulte o *site* [*Contrast Ratio*](https://contrast-ratio.com/#).

---

## Referências

### Design Systems

- [U.S. Web Design System (USWDS)](https://designsystem.digital.gov/)
- [Sistema de Cores do Material Design](https://material.io/design/color/the-color-system.html)
- [Carbon Design System](https://www.carbondesignsystem.com/)
- [Spectrum - Adobe Design System](https://spectrum.adobe.com/)

### Ferramentas e Utilitários

- [Contrast Ratio](https://contrast-ratio.com/)
- [Color Relative Luminance Calculator](https://www.leserlich.info/werkzeuge/kontrastrechner/index-en.php)
- [Color Kit](https://colorkit.io/shades-tints)
- [Paletton](https://paletton.com/)
- [HSL Color Picker](https://hslpicker.com/)
