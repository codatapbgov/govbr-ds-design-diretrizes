# *Slider* (Controle Deslizante)

*Sliders* são controles  deslizantes de entrada de dados que permite ao usuário selecionar um valor limitado por um intervalo.

![Exemplo de componente slider horizontal e vertical.](imagens/diretriz.png)
*Exemplo do componente slider horizontal e vertical.*

Utilize *sliders* quando:

- *o alcance for mais importante do que a precisão.*
Por exemplo, pode ser mais importante para um seletor de preços comunicar onde o preço-alvo está dentro de um determinado intervalo do que o valor exato do produto.

- *quando um valor relativo é mais importante do que um valor exato.*
Por exemplo, um controle deslizante de volume é normalmente mais focado no volume relativo da saída do áudio que no nível específico de decibéis.

---

## Anatomia

| ID | Nome | Referência | Uso |
| ------ | ------ | :------: | ------ |
| 1 | Trilha | --- | Obrigatório |
| 2 | *Label* | [Fundamento Tipografia](/ds/fundamentos-visuais/tipografia) | Opcional |
| 3 | Referências | --- | Opcional |
| 4 | Alça de Controle | --- | Obrigatório |
| 5 | Etiqueta de Valor | --- | Opcional |
| 6 | Caixa de Entrada | [Componente Input](/ds/components/input?tab=designer) | Opcional |
| 7 | Gradações | --- | Opcional |

![Anatomia do componente slider.](imagens/anatomia.png)
*Anatomia do componente slider.*

---

## Detalhamento dos Itens

### 1. Trilha (Obrigatório)

Trilha é o eixo horizontal ou vertical por onde o usuário desliza a alça de controle.

![Trilhas do slider.](imagens/trilha.png)
*Trilhas do componente slider.*

É importante proporcionar *feedback* visual para o usuário auxiliando-o a visualizar o espaço percorrido pela alça de controle na trilha.

![Feedback da trilha do slider.](imagens/trilha-feedback.png)
*Trilha com feedback visual que especifica a quantidade selecionada.*

**Atenção:** o comprimento da trilha do *slider* pode ser personalizado de acordo com o contexto, mas procure evitar trilhas muito longas, pois isso pode dificultar a interação.

### 2. *Label* (Opcional)

O *label* especifica o assunto de que se trata o *slider* e deve estar posicionado sempre alinhado à esquerda e acima da trilha.

![Label](imagens/label-1.png)
*Exemplo de como posicionar corretamente o label no slider.*

Em casos raros em que o contexto é suficiente, o *label* pode ser omitido.

![Exemplo de slider sem label.](imagens/label-2.png)
*Exemplo de caso em que o label não é necessário para o entendimento da função do slider.*

### 3. Referências (Opcional)

As referências podem ser posicionadas nas extremidades da trilha ou abaixo dela, no *slider* horizontal e à direita, no *slider* vertical.

![Posicionamentos das referências.](imagens/referencias-1.png)
*Possíveis posicionamentos das referências do slider.*

Embora sejam opcionais, é importante informar ao usuário os valores máximo e mínimo disponíveis para as referências. Algumas vezes é conveniente informar também o *valor médio* e/ou *valores intermediários*.

![Posicionamentos das referências com valores intermediários.](imagens/referencias-2.png)
*Possíveis posicionamentos das referências do slider com valores intermediários.*

**Atenção:** os valores intermediários das referências devem estar necessariamente posicionados abaixo da trilha, no *slider* horizontal e à direita da trilha, no *slider* vertical. Nestes casos, prefira posicionar os valores mínimos e máximos também abaixo ou à direita para manter a coerência na leitura.

![Posicionamentos das referências com valores intermediários.](imagens/referencias-4.png)
*Possíveis posicionamentos das referências do slider com valores intermediários.*

As referências podem ser *textuais*, *icônicas* ou *interativas* (componente interativo).

![Referências textuais, icônicas e interativas.](imagens/referencias-3.png)
*É possível utilizar texto, ícone ou mesmo um elemento interativo como referências, de acordo com a imagem.*

Se utilizar um elemento interativo como referência, prefira *buttons* com densidade alta.

**Atenção:** às vezes, uma referência precisa ser formatada para localização ou para comunicação mais clara (por exemplo: moedas ou porcentagens). A formatação pode envolver arredondamentos, transformações matemáticas, formatação de números ou exibição de um prefixo ou sufixo (por exemplo: “+/-” ou “px”). Analise se é preciso repensar o posicionamento das referências.

### 4. Alça de Controle (Obrigatório)

A alça de controle é o elemento interativo que o usuário desliza para alcançar um valor. Sempre exibida selecionando um dos valores dentro da distribuição disponível.

Pode ser encontrado em três tamanhos (veja mais adiante Comportamento> Densidade) e é representado graficamente por uma "círculo" com estado *selecionado*, deslizando sobre a trilha.

![Alça de controle.](imagens/alca-controle.png)
*Exemplo de alça de controle.*

### 5. Etiqueta de Valor (Opcional)

A etiqueta de valor indica o valor exato que o usuário está selecionando. Para isso, utiliza-se o componente *tooltip* e, embora seja um elemento opcional, é sempre uma boa ideia utilizá-lo.

A etiqueta de valor deve estar posicionada preferencialmente acima da alça (no *slider* horizontal) ou à esquerda da alça (no *slider* vertical).

Nos casos em que a etiqueta de valor seja utilizada, certifique-se de que o *design* visual do elemento não prejudique a usabilidade. Para telas sensíveis ao toque, considere onde o dedo do usuário será colocado na tela - e quais áreas da tela serão cobertas pelo dedo - enquanto manipula o componente. Embora as etiquetas colocadas diretamente abaixo do controle deslizante possam funcionar em designs de *desktop* usados ​​com um cursor do *mouse*, a mesma colocação de etiquetas não funciona bem para dispositivos móveis e outros designs de tela sensível ao toque porque as etiquetas podem ser ocultadas pelo dedo do usuário enquanto eles estão interagindo com o *slider*.

![Posicionamento da etiqueta](imagens/etiqueta.png)
*Posicione as etiquetas de valor acima ou à esquerda da trilha, de acordo com a variação do slider.*

**Atenção:** a etiqueta deve ficar visível todo o momento em que o usuário estiver interagindo com o componente *slider*.

### 6. Caixa de Entrada (Opcional)

Em algumas situações pode ser interessante disponibilizar uma forma alternativa para o usuário entrar com valores. Nestes casos, disponibilize uma caixa de entrada com o componente *input* integrado ao *slider*.

![Caixa de entrada.](imagens/input.png)
*Exemplo de caixa de entrada.*

Conforme a alça de controle é deslizada o valor correspondente é mostrado na caixa de entrada. Da mesma forma, se o usuário optar por digitar o valor na caixa de entrada, a alça de controle desliza para o valor correspondente na trilha.

### 7. Gradações (Opcional)

Gradações são pequenos espaços na trilha que determinam os valores que podem ser selecionados pelo usuário.

![Gradações na trilha do slider.](imagens/step-2.png)
*Detalhe das gradações na trilha do slider.*

As gradações "forçam" a alça de controle a "saltar" por cada valor previamente definido, impossibilitando que o usuário possa selecionar valores aleatórios.

---

## Tipos

O componente *slider* pode ser encontrado nos tipos *simples* e *composto* e nos formatos *horizontal* e *vertical*. A escolha do tipo e formato mais apropriado deve-se à função e ao *layout*.

### 1. Simples

*Slider* simples é composto por uma única alça de controle e permite a escolha de um valor pertence a um intervalo definido.

![Slider simples.](imagens/tipo-simples.png)
*Exemplo de slider simples no formato horizontal e vertical.*

**Atenção:** geralmente o tipo simples é mais utilizado quando é necessário definir um valor único como: medida, temperatura, volume, etc.

### 2. Composto

*Slider* composto é formado por duas alças de controle e permite a escolha de dois valores pertencentes a um intervalo definido.

![Slider composto.](imagens/tipo-composto.png)
*Exemplo de slider composto no formato horizontal e vertical.*

No tipo composto, o *feedback* visual da trilha deve ocorrer no intervalo entre os dois valores selecionados.

**Atenção:** geralmente o tipo composto é mais utilizado quando é necessário definir uma faixa de valores como: datas, preços, etc.

---

## Comportamentos

### 1. Interação

Há diversas maneiras para o usuário interagir, isto é, selecionar um valor por meio de *sliders*:

#### A. Deslizando a alça de controle

Trata-se da forma mais comum de interação. Basta clicar em cima da alça de controle e deslizá-la para qualquer uma das direções sugeridas pela trilha.

#### B. Por meio do teclado

O *slider* deve ser sempre acessível para o uso por meio de teclas.

- As teclas ↑  → *aumentam* o valor selecionado.

- As teclas ↓ ← *diminuem* o valor selecionado.

#### C. Por meio de componente

É permitido utilizar componentes interativos como referências para facilitar a escolha de valores precisos. Geralmente *buttons* circulares com ênfase terciária* podem ser úteis.

Clicando nos *buttons* o usuário pode aumentar ou diminuir o valor tendo um maior controle da alça de valor.

![Exemplo de slider utilizando buttons.](imagens/buttons.png)
*Exemplo de slider utilizando buttons.*

**Atenção:** embora possa ser utilizado em qualquer ocasião, *buttons* funcionam melhor quando a trilha está construída com valores pré-definidos (*steps*).

#### D. Inserindo o valor exato no campo de entrada

É possível ainda associar um campo de *input* ao componente *slider*. Desta forma, o usuário tem a opção de selecionar o valor desejado por meio do deslocamento da alça de controle (conforme descrito no item 1) ou simplesmente digitando diretamente o valor desejado no campo de entrada.

![Slider com entrada de dados por meio de campo de entrada.](imagens/input-exemples.png)
*Exemplo de slider com entrada de dados por meio de campos de entrada.*

**Atenção:** neste último caso não é necessário o uso de etiqueta de valor, pois o próprio campo de entrada já retorna os valores selecionados.

#### E. Utilizando valores pré-definidos

É possível definir gradações (valores pré-definidos) no componente *slider*. Essa é uma boa maneira de conduzir o usuário para selecionar os valores permitidos.

![Slider com valores pré-definidos.](imagens/step-1.png)
*Exemplo de slider com entrada de valores pré-definidos. Neste exemplo, só é possível selecionar as porcentagens pares.*

![Exemplos de slider simples e composto com valores pré-definidos.](imagens/step-3.png)
*Exemplos de slider simples e composto com valores pré-definidos.*

#### F. Utilizando valores negativos

O *slider* também pode apresentar valores negativos. Nesse caso, é interessante referenciar o ponto zero na metade da trilha.

![Exemplos de slider simples com valores negativos.](imagens/negative.png)
*Exemplos de slider simples com valores negativos.*

**Atenção:** como padrão o *slider* deve vir marcado com valor zero.

### 2. Estados

Os estados previstos para o *slider* são os listados na imagem abaixo. Com exceção do estado desabilitado, todos os demais estados são representados apenas na alça de controle do *slider*.

![Estados](imagens/estados.png)
*Exemplos dos possíveis estados para o slider.*

### 3. Densidades

A densidade do *slider* se resume ao tamanho da alça de controle. A figura seguinte ilustra todas as possibilidades:

![Densidades](imagens/densidades.png)
*Exemplos das densidades do slider.*

**Atenção:** procure utilizar densidade baixa em dispositivos móveis, pois a área de interação com o dedo do usuário é maior tornando-se mais fácil o manuseio.

### 4. Área de Interação

Para facilitar ainda mais a utilização do componente  pelo usuário, a trilha também deve ser interativa. Caso o usuário clique em qualquer parte da trilha, a alça de controle se desloca imediatamente para a posição correspondente.

![Área interativa da trilha](imagens/trilha-interativa.png)
*O usuário pode interagir clicando na trilha.*

Para cada densidade da alça de controle há uma mesma área de interação. Isso auxilia a utilização do componente principalmente em dispositivos com recursos de toque.

Confira em Especificação o valor mínimo (x) sugerido.

![Área interativa](imagens/area-interativa.png)
*Exemplos das áreas interativas para cada densidade da alça de controle.*

### 5. Dispositivos Móveis

Em dispositivos móveis, como telefones e outros aparelhos que possuem o toque como método principal de interação, a alça de controle do *sliders* deve possuir densidade baixa (maior dimensão) para acomodar o uso do dedo ao invés do cursor do *mouse*.

![Dispositivos móveis.](imagens/mobile.png)
*Prefira a dimensão baixa do slider para deixar a alça de controle mais evidente em dispositivos móveis.*

### 6. Fundo Escuro

A imagem a seguir ilustra a representação do componente *slider* em fundos escuros. Confira os *tokens* no final desse documento para o uso correto das cores.

![Fundo escuro.](imagens/fundo-escuro.png)
*Exemplos de aplicação do componente slider em fundo escuro.*

### 7. Acessibilidade

Controles deslizantes funcionam melhor quando o valor específico não é tão importante para o usuário, sendo um valor aproximado o suficiente. **Sempre que o valor exato for importante, o slider pode não funcionar tão bem.**

Por exemplo, se você precisar inserir quantidades como idade ou peso em um formulário, um controle deslizante não seria apropriado, a não ser que venha acompanhado com um campo de *input*.

Outra questão importante ao projetar uma interface de toque é considerar como seria desafiador interagir com um controle deslizante para usuários com dificuldades motoras.

Eles seriam capazes de selecionar o valor exato que desejam? Quanto esforço e quantas tentativas eles precisariam?

Muitos usuários idosos têm mãos menos estáveis ​ e podem encontrar dificuldades em realizar gestos precisos exigidos pelo *slider*.

Não esqueça de permitir que o *slider* seja operado também por meio do teclado.

---

## Melhores Práticas

- Selecionar um valor preciso usando um *slider* pode ser uma tarefa difícil em certas ocasiões e requer alguma habilidade motora. Se a escolha de um valor exato for importante para o objetivo da interface, estude as alternativas para tornar a tarefa mais simples.

- Quanto mais amplo ou mais denso for o intervalo selecionável por meio do *slider*, mais difícil será selecionar um valor preciso.

- Quando o intervalo for muito pequeno (exemplo: 1-3), prefira utilizar outro componente, como *radio* ou *select*. Se for uma configuração binária, use o *switch*.

- Devida à sua versatilidade, o componente *slider* pode ser adaptado à diversas necessidades específicas e funciona bem integrado a outros componentes. Sinta-se livre para testar e assim criar novos componentes mistos.

![Sliders customizados.](imagens/exemplos.png)
*Exemplos de sliders customizados.*

---

## Especificações

### Alinhamento

|Name|Property|Spacing Token|
|--|--|:--:|
|Alça de controle|Vertical|`--spacing-vertical-center`|
|Referências|Horizontal|`--spacing-horizontal-left` / `--spacing-horizontal-right`|
|Referências|Vertical|`--spacing-vertical-bottom`|

### Espaçamento

|Name|Property|Token/Value|
|--|--|:--:|
|Referências|margin-right/margin-left|`--spacing-scale-half`|
|Label|margin-botton|`--spacing-scale-base`|
|Caixa de Entrada|margin-right/margin-left|`--spacing-scale-2x`|
|Gradações|margin-right|`1px`|

### Dimensão

|Name|Property|Token/Value|
|--|--|:--:|
|Alça de controle (densidade alta)|width/height|`--spacing-scale-2x`|
|Alça de controle (densidade média)|width/height|`--spacing-scale-2xh`|
|Alça de controle (densidade baixa)|width/height|`--spacing-scale-3x`|
|Trilha|height|`--spacing-scale-half`|
|Área mínima de interação (x)|width/height|`--spacing-scale-4x`|

### Cor

|Name|Property|Color Token|Opacity
|--|--|--|:--:|
|Alça de controle|background|`--blue-warm-vivid-50`|-|
|Alça de controle (fundo escuro)|background|`--blue-warm-vivid-50`|-|
|Trilha|background|`--gray-20`|-|
|Trilha (fundo escuro)|background|`--pure-0`|-|
|Trilha marcada|background|`--blue-warm-vivid-50`|`--surface-opacity-md`|
|Trilha marcada (fundo escuro)|background|`–--blue-warm-vivid-50`|`--surface-opacity-md`|
|Label / Referências|text|`--gray-80`|-|
|Label / Referências (fundo escuro)|text|`--pure-0`|-|
|Ícones|icon|`--gray-80`|-|
|Ícones (fundo escuro)|icon|`--pure-0`|-|

### Tipografia

|Name|Property|Token/Value|
|--|--|:--:|
|Label|size|`--font-size-scale-base`|
|Label|font-weight|`--font-weight-semi-bold`|
|Referências|size|`--font-size-scale-base`|
|Referências|font-weight|`--font-weight-regular`|
