# Tema

Temas são customizações dos elementos do *Design System* com a finalidade de adaptar o *layout* a uma determinada marca ou propósito sem com isso perder as características básicas que as tornam reconhecíveis como pertencentes ao mesmo *Design System*.

Este documento tem por finalidade orientar o *designer* para a criação de temas condizentes com as premissas do Design System.

![exemplos de componentes tematizados.](imagens/tema-exemplos.png)
*Exemplos de componentes tematizados.*

Crie um tema para atender necessidades visuais específicas. Tenha em mente que embora haja uma nova definição das características fundamentais do *Design System* a interface ainda precisa ser reconhecida como pertencente ao Design System. Portanto, altere somente os elementos que forem realmente necessários e que tragam valor para o usuário.

Lembre-se de que quanto maior a customização, mais distante o tema ficará em relação ao tema Padrão (*Default*) e isso pode ocorrer em falta de conexão e identidade com o Design System. Por isso que, atualmente, só é possível criar temas customizando o fundamento cor.

---

## Acessibilidade

Nesse documento menciona diversas vezes o contraste entre cores e a relação entre o primeiro plano e o plano de fundo. Dentro do Design System utilizamos diretrizes de acessibilidade da [WCAG 2](https://www.w3.org/TR/WCAG20/) como guia, e para tal é definido o *nível AA* como o *mínimo* de acessibilidade aceitável.

Para maiores detalhes consulte o [Fundamento Cores](/fundamentos-visuais/cores).

É de fundamental importância compreender as necessidades de cada usuário. Pessoas com dificuldades visuais podem ter suas próprias dificuldades e é importante considerar essas necessidades ao criar um tema.

Algumas ferramentas podem ser utilizadas para checar o contaste das cores para diferentes tipos de usuários:

- [Cálculo de luminância](https://contrastchecker.online/color-relative-luminance-calculator)
- [Contraste de cores](https://contrast-ratio.com/)
- [Teste de Daltonismo](https://color.adobe.com/pt/create/color-accessibility)

---

## Orientações para Criação de Tema

### Introdução

Para criar um novo tema é necessário entender a relação que os elementos e a função exercem na interface. Portanto, é de extrema importância que o *designer* esteja familiarizado com os fundamentos visuais que forem customizados.

Entenda que os atributos que não forem modificados em um novo tema, herdarão as configurações do tema *Default* do Design System. Utilize isso a seu favor, alterando somente os atributos que fizerem sentido em seu projeto.

Para haver mais personalidade e flexibilidade no *layout*, é possível definir cores *principais* e *alternativas* para cada elemento ou função, diferenciando o uso em fundos claros e escuros:

- **Cor Principal:** cor recomendada para o uso comum dentro dos elementos e/ou funções. As funções serão descritas mais adiante.
- **Cor Alternativa:** cores que podem ser utilizadas para conferir mais ênfase a um determinado elemento ou função, proporcionando maior versatilidade. Quando adotar uma cor alternativa para elementos em fundos claros, adote também versões correspondentes para fundos escuros. É possível existir quantas cores alternativas forem necessárias, porém, não é recomendável exagerar na quantidade. Quanto mais cores alternativas forem utilizadas, menos consistente o *layout* ficará.
- **Fundo Claro e Fundo Escuro**: um tema pode ter predominância de um determinado fundo específico. Porém, o ideal é que haja flexibilidade de uso de fundos distintos em situações especificas, a fim de conferir ênfases e hierarquia dos elementos da interface. Para mais detalhes, veja a seção seguinte em Funções Temáticas.

![Cores principais e secundárias em fundo claro e escuro.](imagens/funcao.png)
*Exemplos de cores principais e secundárias em fundo claro e escuro.*

**Atenção:** nem todos os elementos ou funções necessitam de uma cor alternativa. Utilize-as apenas quando houver problemas de usabilidade com a cor principal, quando for necessário destacar uma funcionalidade na tela ou simplesmente quando fizer sentido no *layout*. Entenda com profundidade sua paleta de cores a fim de definir as alternativas necessárias para suas funções dentro da interface.

### Funções Temáticas

É detalhado a seguir os principais elementos customizáveis na criação de um tema baseados nas funcionalidades da interface.

#### 1. Função *Container* (Superfícies)

Superfície é qualquer forma indivisível que pode conter um ou mais elementos funcionando como um *container*. Como conceito geral, qualquer plano de fundo (*background*) de um elemento pode ser entendido como a superfície da interface. Consulte o documento [Fundamentos Visuais > Superfície](/fundamentos-visuais/superficie) para maiores informações.

As cores de superfície são utilizadas como base cromática para composição do tema e compõem harmonicamente com as demais cores. São utilizadas especificamente em planos de fundo nas telas da interface e superfícies de componentes. As cores para as superfícies (claras, escuras e alternativas) devem ter pouca interferência das demais cores da paleta cromática do tema.

É fundamental considerar o contraste com os elementos que estarão sobre as superfícies (principalmente tipográficos, iconográficos e interativos). Exatamente por possuir função tão ampla é sugerida a utilização de cores neutras ou cores com baixa saturação (seja pelo próprio matiz ou pela baixa percepção utilizada pela quantidade muito alta ou baixa de luminosidade). Ainda assim é possível utilizar cores alternativas mais saturadas para destacar pontos específicos da interface. Se for este o caso, faça com bastante cautela.

- **Fundo Claro:** qualquer cor até o grau 40 é considerada uma cor clara.
- **Fundo Escuro:** qualquer cor acima do grau 40 é considerada uma cor escura.

![Cores claras e escuras.](imagens/cores-claras-escuras.png)
*Cores claras e escuras de uma família cromática.*

**Recomendações:**

- Use cores neutras:
- Família Pura: `Pure`;
- Famílias de Cinza: `Gray`, `Gray-Cool` e `Gray-Warm`;
- Use cores com graus de 0 a 10 para fundos claros;
- Use cores com graus de 70 a 100 para fundos escuros;
- A fim de oferecer maior flexibilidade ao *layout* é interessante considerar um tema que ofereça superfícies para *fundos claros* e *fundos escuros*.
- *Cores alternativas* são bastante recomendadas para essa função. Prefira cores da mesma família da cor principal.

![Cores das famílias pure, gray, gray cool e gray warm.](imagens/color-superficie-1.png)
*Cores das famílias pure, gray, gray-cool e gray-warm.*

![Graus sugeridos para superfícies.](imagens/color-superficie-2.png)
*Os graus sugeridos para cores de superfícies estão em destaque em laranja.*

**Atenção**: outros elementos descritos no fundamento superfície podem seguir as mesmas recomendações listadas acima. Isso inclui bordas, linhas ou qualquer elemento que auxilia a diagramação de uma interface. Estes elementos não necessitam de destaque e devem ser visualizados de forma discreta na tela.

#### 2. Função Leitura (Tipografia e Iconografia)

A tipografia e a iconografia são os elementos muito importantes em uma interface pois, juntamente com as imagens, são os responsáveis pelo conteúdo de uma interface. Consulte os documentos [Fundamentos Visuais > Tipografia](/fundamentos-visuais/tipografia) e [Fundamentos Visuais > Iconografia](/fundamentos-visuais/iconografia) para maiores informações.

São cores utilizadas nos elementos de leitura como parágrafos, títulos, *labels*, etc. A função principal dessas cores é proporcionar contraste de leitura com as superfícies definidas no passo anterior, oferecendo um bom nível de leiturabilidade. Logo, para toda cor de superfície há uma cor de leitura correspondente.

Ao definir as cores para a função leitura busque sempre o máximo contraste possível entre os elementos de leitura e a área de fundo (superfície). É  possível customizar separadamente cada função de texto. Por exemplo: podemos utilizar cores diferentes para os estilos H1, H2, H3, ..., H6, parágrafo, *label*, *legend*, *placeholder*, *input*. Tenha o cuidado para não definir cores já utilizadas em outras funções.

**Recomendações:**

- Use cores neutras (principalmente para textos longos);
- Família Pura: `Pure`;
- Famílias de Cinza: `Gray`, `Gray-Cool` e `Gray-Warm`;
- Use cores com graus de 0 a 10 em fundos escuros;
- Use cores com graus de 70 a 100 em fundos claros;
- É interessante considerar cores principais que gerem contraste nos tipos de superfícies criadas. Principalmente para *fundos claros* e *fundos escuros*.
- *Cores alternativas* podem ser utilizadas para personalizar os estilos tipográficos. Prefira cores da mesma família da cor principal.

![Cores das famílias pure, gray, gray cool e gray warm.](imagens/color-superficie-1.png)
*Cores das famílias pure, gray, gray-cool e gray-warm.*

![Graus sugeridos para cores de leitura.](imagens/color-superficie-2.png)
*Os graus sugeridos para cores de leitura estão em destaque em laranja.*

#### 3. Função *Feedback* (Estados)

Estados são representações visuais para *feedbacks* interativos ou informacionais relacionadas aos elementos na interface. Portanto, é preciso haver muito cuidado ao definir estas cores, pois a cor escolhida para o estado interativo, por exemplo, fornecerá a consistência e a personalidade em todo o *layout* do seu projeto. Consulte o documento [Fundamentos Visuais > Estados](/fundamentos-visuais/estados).

Existem dois grupos principais de estados: *interação* e *avisos*. Não é recomendável a personalização de alguns tipos pois são *feedbacks* fortemente reconhecidos pela boa parte dos usuários e relacionadas às funções específicas:

- **Interação**: Ligado/Desligado.
- **Avisos**: Erro, Sucesso, Alerta e Informativo.

Devida à forte relação entre cor e função é interessante evitar o uso das famílias ligadas ao *grupo aviso* em outros contextos:

- **Erro:** `Red Vivid`;
- **Sucesso:** `Green Cool Vivid`;
- **Alerta:** `Yellow Vivid`;
- **Informativo:** `Blue Warm Vivid`.

**Atenção:** se for realmente necessário, é possível ajustar as tonalidades das cores dos estados do grupo aviso a fim de melhor se ajustarem ao *layout*. Porém, busque alternativas dentro das famílias já utilizadas pelo tema *default*.

![Cores estado grupo aviso.](imagens/color-aviso.png)
*Evite utilizar as cores e as famílias de cores do grupo aviso para outros elementos ou funções.*

As cores de estados que permitem serem personalizadas são as do *grupo interação*.

A sugestão é eleger uma família de cor, e a partir dela aplicar as cores dessa família ao longo dos estados e/ou utilizá-la como referência para alguns estados.

**Recomendações:**

- Prefira famílias de cores do **círculo cromático vivid**, por possuir maior saturação e maior destaque na tela.
- Prefira famílias que gerem contraste em relação às funções *container* e de leitura.

Após determinar a família, escolha a cor do estado interativo que será a base para a maioria dos estados.

##### Estado Interativo

A cor do estado interativo indica a possibilidade de interação de qualquer elemento presente na tela. É necessário que esta cor se destaque em relação os demais elementos, *backgrounds* ou informações textuais ficando claro para o usuário que qualquer elemento que apresente a cor interativa possua algum tipo de interação.

Portanto, a cor interativa é a que mais deve se destacar no *layout*.

**Recomendações:**

- Prefira um grau intermediário com o matiz forte (graus de 40 a 60 costumam funcionar). Em caso de dúvida, utilize o *grau 60*.

![Sugestão de cores para o estado interativo.](imagens/color-interativa.png)
*Cores sugeridas para utilização como cor principal do estado interativo no círculo cromático vivid.*

- Pode ser necessária a definição da cor alternativa para maior flexibilidade de uso no *layout* (geralmente uma cor alternativa é o suficiente). Opte por uma cor bem distinta da principal. Sugerimos optar por uma cor de uma *família análoga em 6 níveis* distante da família da cor principal no círculo cromático.

![Sugestão de cores alternativas.](imagens/color-auxiliar.png)
*Cores sugeridas para utilização como cor alternativa no círculo cromático vivid. A família marcada com o ponto amarelo representa a família da cor principal. Os pontos verdes sugerem opções de cores alternativas.*

- Considere o uso com fundos/texto claros e escuros (tanto na cor principal como na alternativa). Para isso, procure uma variação dentro de uma mesma família.

##### Estado Ativo

A cor do estado ativo indica, de um conjunto de opções, qual item está sendo visualizado no momento e compreende duas cores: uma sempre será `Pure 0` (cor branca) e a outra, uma cor baseada na mesma família do estado interativo.

**Recomendações:**

- Prefira cores mais escuras em relação à cor escolhida para o estado interativo (recomenda-se *um grau acima*).

![Sugestão de cores para o estado ativo.](imagens/color-ativa.png)
*A imagem sugere que se o grau escolhido para a cor do estado interativo for 60, o grau para a cor do estado ativo deve ser 70.*

- Não é recomendável o uso de cor alternativa para o estado ativos pois o usuário poderá se sentir confuso caso haja muitos *feedbacks* na interface. O *feedback* de um estado ativo deve ser o mais consistente possível dentro no sistema.

- Uso de fundo claro e escuro pode ser utilizado invertendo as duas cores escolhidas para o estado ativo.

##### Estado Selecionado

A cor do estado selecionado indica uma ou mais opções selecionadas pelo usuário.

**Recomendações:**

- Prefira cores mais claras em relação à cor escolhida para o estado interativo (recomenda-se *dois graus abaixo*).

![Sugestão de cores para estado selecionado.](imagens/color-selecionado.png)
*A imagem ilustra a escolha do grau 40 para a cor do estado selecionado, dois níveis abaixo do grau 60 (cor do estado interativo).*

- Não é recomendável cores alternativas para o estado selecionado, pois o usuário pode se sentir confuso caso haja muitos *feedbacks* na interface. O *feedback* de um estado selecionado deve ser o mais consistente possível dentro no sistema.

**Atenção:** geralmente é possível resolver as variações do estado selecionado com apenas uma cor. Para isso, é recomendado que a cor tenha uma luminância intermediária, funcionando bem em fundos claros e escuros (grau 40 costuma funcionar satisfatoriamente na maioria dos casos).

##### Estado Foco

O estado foco ocorre quando o usuário utiliza o teclado, o *mouse* ou algum método de entrada de voz para destacar um elemento interativo. Por esse motivo, é necessário que haja alta ênfase em relação aos demais elementos da interface.

Diferentemente dos outros estados descritos, com o foco algumas regras são mais restritivas, pois fazem parte de práticas mais específicas de acessibilidade.

**Recomendações:**

- Utilize uma *família de cores complementares* à família do estado interativo. Pode-se optar também por *famílias análogas* à complementar (até quatro níveis). Com isso, há nove famílias disponíveis para escolha. Trabalhe com a opção que melhor adequa-se ao seu *layout* porém, utilize apenas *uma única opção de cor* para o estado foco ao longo de todo o projeto.

![Sugestão de cores para estado foco.](imagens/color-foco.png)
*Cores sugeridas para utilização para o estado foco no círculo cromático vivid. A família sinalizada com o ponto verde é complementar à cor interativa (ponto amarelo) e está diametralmente oposta no círculo cromático. As quatro famílias ao lado direito e esquerdo da família da cor complementar são as cores análogas e também estão disponíveis para uso no estado foco.*

- A cor escolhida deve apresentar contraste de no mínimo 7.1 em relação ao fundo onde ocorre.
- Escolha duas cores de foco (dentro da mesma família) para ser aplicado no fundo claro e escuro. Prefira uma opção que funcione nos dois fundos ao mesmo tempo.
- Evite o uso de cores que se assemelham à cor escolhida para o estado interativo e às cores utilizadas no estado de grupo de aviso, principalmente a cor de erro.

##### Demais estados

Os demais estados interativos utilizam outros recursos para *feedback* visual, como opacidade, ou são baseados na cor do estado interativo. Portanto, não há a necessidade de personalização.

---

## Passo a Passo Para a Criação de um Tema

Baseado no que foi dito acima, criaremos juntos um novo tema para aplicação em uma interface. Vamos denominá-lo de *Tema Experimental*.

**Lembrando:**

- Os temas afetam toda a interface do usuário, incluindo componentes individuais como *buttons*, *messages*, *cards*, etc.;
- Consulte sempre o documento [Fundamentos Visuais > Cores](/fundamentos-visuais/cores) juntamente com este passo a passo para criar um tema consistente para seu projeto.
- Considere os conceitos de usabilidade/acessibilidade apresentados nas diretrizes desse *Design System*.
- Considere criar versões para os elementos em fundos claros e escuros. Algumas vezes a mesma cor pode funcionar adequadamente em ambos os fundos.
- Pode ser definido uma ou mais cores alternativas para cada cor principal, desde que sejam justificáveis os usos.
- Caso haja definição de fundo claro e fundo escuro, deve haver também para as cores alternativas (se for o caso).

### Passo 1: Defina as Cores das Superfícies

Para o Tema Experimental, definimos quatro possibilidades de cores para superfícies e a borda. Como cor principal para superfícies com fundo claro optamos pela cor branca, pois necessitamos de grande espaço para textos e outros elementos de leitura e desejamos o maior contraste possível. Como alternativa, optamos por um cinza claro para ser utilizado em superfícies com um certo nível de destaque. Finalmente, para superfícies escuras optamos por um cinza escuro a fim de conferir contraste com o fundo claro e uma versão de cinza ainda mais escuro como cor alternativa.

A tabela abaixo resume as escolhas:
|Elemento|Color Name|Color|Token|
|--|--|:--:|:--:|
|Superfície clara|`Pure 0`|![Paletas](imagens/color-pure-0.png)|`--background`|
|Superfície clara (alternativa)|`Gray 5`|![Paletas](imagens/color-gray-5.png)|`--background-alternative`|
|Superfície escura|`Gray 70`|![Paletas](imagens/color-gray-70.png)|`--background-dark`|
|Superfície escura (alternativa)|`Gray 90`|![Paletas](imagens/color-gray-90.png)|`--background-dark-alternative`|

Para representar os elementos auxiliares (bordas, linhas, etc.) optamos pela cor `Gray 20` em fundos claros e `Gray 50` em fundos escuros por ser sutis e discretas.

A tabela abaixo resume as escolhas para as cores dos elementos auxiliares:

|Componente|Superfície|Color Name|Color|Token|
|--|--|--|:--:|:--:|
|Elementos auxiliares|Clara|`Gray 20`|![Paletas](imagens/color-gray-20.png)|`--border`|
|Elementos auxiliares|Escura|`Gray 50`|![Paletas](imagens/color-gray-50.png)|`--border-dark`|

### Passo 2: Defina as Cores para a Função Leitura (Tipografia e Iconografia)

Para o Tema Experimental vamos definir uma única cor para todo tipo de texto em superfície clara, superfície escura e uma cor diferenciada para os títulos H1 em superfície clara e escura, pois desejamos que o título se destaque dos demais elementos de leitura.

Para superfícies claras mantemos o cinza escuro `Gray 80` do tema *default* por oferecer um excelente contraste em relação a superfície clara e não possui tanto brilho como o preto que pode incomodar a leitura em uma grande massa de texto. Para superfícies escuras também mantemos o branco do tema *default*. Para os títulos principais das telas (H1) decidimos optar por uma cor distinta, a fim de conferir destaque aos títulos. Para isso optamos pelas cores `Mint Vivid 60` e `Mint Vivid 10`.

A tabela abaixo resume nossas escolhas:
|Tipografia|Color Name|Color|Token|
|--|:--:|:--:|:--:|
|Texto (fundo claro)|`Gray 80`|![Paletas](imagens/color-gray-80.png)|`--color`|
|Texto (fundo escuro)|`Pure 0`|![Paletas](imagens/color-pure-0.png)|`--color-dark`|
|Título H1 (fundo claro)|`Mint Vivid 60`|![Paletas](imagens/color-mint-vivid-60.png)|`--h1`|
|Título H1 (fundo escuro)|`Mint Vivid 10`|![Paletas](imagens/color-mint-vivid-10.png)|`--h1-dark`|

### Passo 3: Defina as Cores para a Função *Feedback* (Estados)

#### 1. Estado Interativo

No Tema Experimental a cor magenta é uma cor de destaque da marca (de um produto hipotético) e por isso, escolhemos um grau intermediário (*grau 60*) da família `Magenta Vivid` do círculo cromático `Vivid` para representar o estado interativo. Para garantir um bom contraste em fundos escuros utilizaremos a mesma cor, porém com *grau 50*.

A tabela abaixo resume as escolhas para cor principal do estado interativo em superfícies claras e escuras:
|Color Name|Superfície|Color|Token|
|--|:--:|:--:|:--:|
|`Magenta Vivid 60`|Clara|![Paletas](imagens/color-magenta-vivid-60.png)|`--interactive`|
|`Magenta Vivid 50`|Escura|![Paletas](imagens/color-magenta-vivid-50.png)|`--interactive-dark`|

Definimos também uma cor alternativa para cada uma das superfícies. Utilizando a mesma dica sugerida anteriormente temos na cor `Blue Warm Vivid 60`. Assim como fizemos para a cor principal, definimos a cor  `Blue Warm Vivid 50` para fundos escuros.

A tabela abaixo resume as escolhas para cores alternativas do estado interativo em superfícies claras e escuras:
|Color Name|Superfície|Color|Token|
|--|:--:|:--:|:--:|
|`Blue Warm Vivid 60`|Clara|![Paletas](imagens/color-bluewarm-vivid-60.png)|`--interactive-alternative`|
|`Blue Warm Vivid 50`|Escura|![Paletas](imagens/color-bluewarm-vivid-50.png)|`--interactive-dark-alternative`|

#### 2. Estado Ativo

Seguindo a recomendação, em nosso caso, a cor do estado interativo é `Magenta Vivid 60` e, portanto, aumentando um grau teremos a cor `Magenta Vivid 70`, além do branco.

A tabela abaixo resume as escolhas para as cores principais do estado ativo em superfícies claras e escuras:
|Color Name|Color|Token|
|--|:--:|:--:|
|`Magenta Vivid 70`|![Paletas](imagens/color-magenta-vivid-70.png)|`--active`|
|`Pure-0`|![Paletas](imagens/color-pure-0.png)|`*`|

Como recomendado, não adotamos cores alternativas para o estado ativo.

#### 3. Estado Selecionado

Para o estado selecionado, optamos pela cor `Magenta Vivid 40` tanto para fundos claros como para fundos escuros.

A tabela abaixo resume as escolhas para a cor do estado selecionado:
|Color Name|Superfície|Color|Token|
|--|:--:|:--:|:--:|
|`Magenta Vivid 40`|Clara|![Paletas](imagens/color-magenta-vivid-40.png)|`--selected`|
|`Magenta Vivid 40`|Escura|![Paletas](imagens/color-magenta-vivid-40.png)|`--selected`|

Como recomendado, não adotamos cores alternativas para o estado selecionado.

#### 4. Foco

Consideramos a cor complementar à cor interativa (`Magenta Vivid 60`), ou seja, a cor `Mint Vivid 60`. Para fundos escuros, descemos um grau e temos a cor `Mint Vivid 50` que é a cor complementar à cor `Magenta Vivid 50`.

A tabela abaixo resume as escolhas para as cores do estado foco:
|Color Name|Superfície|Color|Token|
|--|:--:|:--:|:--:|
|`Mint Vivid 60`|Clara|![Paletas](imagens/color-mint-vivid-60.png)|`--focus`|
|`Mint Vivid 50`|Escura|![Paletas](imagens/color-mint-vivid-50.png)|`--focus-dark`|

---

## Melhores Práticas

- As cores para o *design* da interface são normalmente escolhidas de acordo com o esquema de cores da marca ou necessidades estéticas e estilísticas do projeto. A psicologia da cor também deve ser considerada em todas as decisões, pois desempenha um papel influente no *design*. Considere todas essas questões ao definir as cores de um tema.

- Esteja atento para garantir o contraste mínimo em relação às cores escolhidas. Se for necessário, faça ajustes nas tonalidades (variando os graus dentro de uma mesma família de cores) para atingir o contraste pretendido.

- As cores escolhidas devem possuir uma relação mínima de contraste em relação à cor de fundo de *3:1*, como definido pelas normas da [WCAG 2](https://www.w3.org/TR/WCAG20/).

---

## Exemplo de *Layout do Template Base* Utilizando o Tema Experimental

A seguir, utilizamos o Tema Experimental que acabamos de criar para customizar uma tela de exemplo. É importante conferir se as cores estão funcionando de modo global no *layout*.

![Exemplo de template consumindo Tema Novo](imagens/template.png)
*Exemplo de template consumindo o Tema Experimental de exemplo.*

---

## Especificações

### Superfície

|Tipo|Token|
|---|---|
|Cor Principal|`--background`|
|Cor Alternativa|`--background-alternative`|
|Cor Escura Principal|`--background-dark`|
|Cor Escura Alternativa|`--background-dark-alternative`|
|Cor Principal (Borda)|`--border`|
|Cor Alternativa (Borda)|`--border-alternative`|
|Cor Escura Principal (Borda)|`--border-dark`|
|Cor Escura Alternativa (Borda)|`--border-dark-alternative`|

### Leitura (Tipografia e Iconografia)

|Tipo|Token|
|---|---
|Cor Principal|`--color`|
|Cor Alternativa|`--color-alternative`|
|Cor Escura Principal|`--color-dark`||
|Cor Escura Alternativa|`--color-dark-alternative`|
|Cor Principal (h1)|`--h1`|
|Cor Escura Principal (h1)|`--h1-dark`|
|Cor Principal (h2)|`--h2`|
|Cor Escura Principal (h2)|`--h2-dark`|
|Cor Principal (h3)|`--h3`|
|Cor Escura Principal (h3)|`--h3-dark`|
|Cor Principal (h4)|`--h4`|
|Cor Escura Principal (h4)|`--h4-dark`|
|Cor Principal (h5)|`--h5`|
|Cor Escura Principal (h5)|`--h5-dark`|
|Cor Principal (h6)|`--h6`|
|Cor Escura Principal (h6)|`--h6-dark`|

### *Feedback* (Estados)

|Tipo|Token|
|---|---|
|Cor Principal (Interativo)|`--interactive`|
|Cor Alternativa (Interativo)|`--interactive-alternative`|
|Cor Escura Principal (Interativo)|`--interactive-dark`|
|Cor Escura Alternativa (Interativo)|`--interactive-dark-alternative`|
|Cor Principal (Informativo)|`--info`|
|Cor Alternativa (Informativo)|`--info-alternative`|
|Cor Principal (Sucesso)|`--success`|
|Cor Alternativa (Sucesso)|`--success-alternative`|
|Cor Principal (Alerta)|`--warning`|
|Cor Alternativa (Alerta)|`--warning-alternative`|
|Cor Principal (Erro)|`--danger`|`--red-vivid-50`|
|Cor Alternativa (Erro)|`--danger-alternative`|
|Cor Principal (Foco)|`--focus`|
|Cor Escura Principal (Foco)|`--focus-dark`|
|Cor Principal (Selecionado)|
|Cor Principal (Ativo)|`--active`|
